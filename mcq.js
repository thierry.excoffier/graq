
function request(url, callback)
{
    xmlhttp = new XMLHttpRequest();
    xmlhttp.the_url = url ;
    xmlhttp.open("GET", url + (S ? S.ticket : window.location.search), true) ;
    xmlhttp.setRequestHeader("If-Modified-Since", "Thu, 1 Jan 1970 00:00:00 GMT") ;
    xmlhttp.setRequestHeader("Cache-Control", "no-cache") ;
    xmlhttp.onreadystatechange = function()
        {
            if (this.readyState != 4 )
                return ;
            if (this.status == 0)
                {
                    console.log(url + "\nstatus=0") ;
                    window.location = window.location ;
                    return ;
                }
            if (this.readyState == 4 && this.status == 200)
                {
                    if ( this.responseText === '' )
                        {
                            console.log(url + "\nresponseText=''") ;
                            window.location = window.location.replace(/[?]ticket=[^&]*/, '') ;
                            return ;
                        }
                    callback(this.responseText) ;
                    
                }
        }
    xmlhttp.send() ;
}

function raw(t)
{
    if ( t.replace === undefined )
        return t.toString() ; // Number
    return  t.replace(/&/g, '&amp;')
    .replace(/>/g, '&gt;')
    .replace(/</g, '&lt;')
    .replace(/﹛/g, '{')
    .replace(/﹜/g, '}') ;
}

function html(t)
{
    t = raw(t)
            .replace(/\n *\n/g, '\n<p>\n')
            .replace(/\*\*([^*]*)\*\*/g, '<b>$1</b>')
            .replace(/__([^_]*)__/g, '<u>$1</u>')
            .replace(/\n *\*([^\n]*)/g, '<item>$1</item>')
            .replace(/(https?:\/\/[^ \n,"»]*)/g, '<a href="$1">$1</a>')
            .replace(/(«[^»]*) /g, '$1<m> </m>')
            ;
    var before ;
    while( t != before )
        {
            before = t ;
            t = t.replace(/(«[^»]*) /g, '$1<m> </m>') ;
        }

    for(var i in S.params.replacements)
        t = t.replace(new RegExp(S.params.replacements[i][0]), S.params.replacements[i][1]) ;
    return t ;
}

function Session(session)
{
    this.params = session[0] ;
    for(k in this.params)
        if ( Number(this.params[k]) === this.params[k] )
            this.params[k] = Number(this.params[k]) ;
    this.history = session[1] ;
    this.dt = 40 ;
    this.start_day = this.params.start_time_tuple[6] ;
    this.real_start = this.params.start_time - ((this.start_day+1) % 7) * 86400 ;
    this.slot_x = Number(this.params.slot_x) ;
    this.slot_y = Number(this.params.slot_y) ;
    this.debug = Number(this.params.debug) ;
    this.show_the_clock = true ;
    this.show_the_abcd = true ;
    this.holidays = this.params.holidays ;
    this.params.fontFamily = this.params.fontFamily || 'sans-serif' ;
    this.state = '' ;
}

///////////////////////////////////////////////////////////////////////////////
// State change functions
// The name is __'old state'__'new state'
// If the method is not defined, the state change is not allowed
///////////////////////////////////////////////////////////////////////////////

Session.prototype.____message = function()
{ this.message.innerHTML = "Cliquez au centre de l'écran pour démarrer" ; } ;
Session.prototype.__initial__message = function()
{ this.message.innerHTML = '<button class="start">GO !</button>' ; } ;
Session.prototype.__message__initial = function()
{ this.message.innerHTML = '' ; } ;
Session.prototype.__asking__midnight = function()
{ this.message.innerHTML = "<b>Attendez minuit pour avoir d'autres questions</b>" ; };
Session.prototype.__answer__initial = function() {} ;
Session.prototype.__midnight__initial = function() {} ;

Session.prototype.__message__asking = function()
{
    if ( this.ended )
        return "C'est fini, plus de questions.\nTravaillez avec autre chose." ;
    var THIS = this ;
    function wait_question(data)
    {
        data = JSON.parse(data) ;
        if ( data.day === undefined )
            {
                THIS.set_state('midnight') ;
                return ;
            }
        THIS.set_state('question', data) ;
    } ;
    window.scrollTo(0, 0)
    if ( this.question_id.length == 2 )
        question_id = '/' + decodeURIComponent(this.question_id[1].split('&')[0]) ;
    else
        question_id = this.click_on_holiday ? '/1' : '/0';

    request('start.json' + question_id + this.ticket, wait_question) ;
} ;
Session.prototype.__asking__question = function(data)
{
    this.slot = this.get_slot(data.day, data.slot) ;
    if ( data.key )
        this.title.textContent = data.key ;
    this.nr_choices = data.choices.length ;
    this.nr_history = data.history ;
    this.keywords = data.keywords ;
    this.clock_time = data.time * (data.time_scale || 1) * 1000 ;
    this.collapse_spaces = data.collapse_spaces ;
    var t = ['<span class="hate"></span>', html(data.question)] ;
    if ( this.keywords )
        {
            this.answer = [] ;
            this.choices = data.choices ;
            var x = [], width = 0 ;
            this.allowed_space = this.allowed_enter = false ;
            for(var i in data.choices)
                {
                    if ( data.choices[i] == '\n' )
                        this.allowed_enter = Number(i) ;
                    if ( data.choices[i] == ' ' )
                        this.allowed_space = Number(i) ;
                    i = data.choices[i].replace(/\n/g, '«Entrée»') ;
                    i = raw(i) // HTML not allowed in keywords
                    if ( ! data.keep_space )
                        i = i.replace(/ /g, '<m> </m>') ;
                    x.push(i) ;
                    width = Math.max(width, i.replace(/<[^>]*>/g, '').length) ;
                }
            var nr_cols = Math.max(1, Math.floor(31 / (width + 3))) ;
            if ( nr_cols > 3 && x.length == 9 )
                nr_cols = 3 ;
            if ( nr_cols > 4 && (x.length == 12 || x.length == 16) )
                nr_cols = 4 ;
            t.push('<table>') ;
            for(var i in x)
                {
                    if ( i % nr_cols == 0 )
                        t.push('<tr>') ;
                    t.push('<td>') ;
                    t.push('<button class="choice') ;
                    t.push(i) ;
                    t.push('"><span class="letter">') ;
                    t.push(String.fromCharCode(65 + Number(i))) ;
                    t.push(': </span>') ;
                    t.push(x[i]) ;
                    t.push('</button>') ;
                    if ( i % nr_cols == nr_cols-1 )
                        t.push('</tr>') ;
                }
            t.push('</table><button class="backspace">⌫</button>') ;
            t.push('<br><div><span class="hate"></span><div id="answer"></div></div>') ;
            t.push('<br><div><span class="hate"></span><div id="good_answer"></div></div>') ;
            t.push('<br><button class="send"><span class="letter">') ;
            t.push(String.fromCharCode(65 + Number(i) + 1)) ;
            t.push(': </span>Envoyer la réponse</button>') ;
        }
    else
        {
            t.push('<ol type="A">') ;
            for(var i in data.choices)
                t.push('<li class="choice' + i + '"><span class="hate"></span>' + html(data.choices[i])) ;
            t.push('</ol>') ;
        }
    this.question.innerHTML = t.join('') ;
    this.update_answer() ;
    this.clock_start() ;
} ;
Session.prototype.__question__grading = function(choice)
{
    if ( this.keywords )
        {
            while ( this.answer.length && this.answer[this.answer.length-1] === this.allowed_space )
                this.answer.pop() ;
            while ( this.answer.length && this.answer[0] === this.allowed_space )
                this.answer.shift() ;
            if ( this.collapse_spaces ) // ' ' + ' ' → ' '
                for(var i = 1 ; i < this.answer.length - 2 ; i++)
                    while ( this.answer[i] === this.allowed_space && this.answer[i+1] === this.allowed_space )
                        this.answer.splice(i+1, 1);
            for(var i = 0 ; i < this.answer.length - 2 ; i++) {
                var val = this.choices[this.answer[i]];
                while (this.answer[i+1] === this.allowed_space
                     && val.length > 2 && val.substr(val.length-1) == ' ')
                        // 'XXX ' + ' ' → 'XXX '
                        this.answer.splice(i+1, 1);
                while (this.answer[i-1] === this.allowed_space
                    && val.length > 2 && val.substr(0, 1) == ' ')
                        // ' ' + ' XXX' → ' XXX'
                        this.answer.splice(--i, 1);
                }
            this.update_answer() ;
            this.answer_sent = this.answer.join(',') ;
        }
    else
        this.answer_sent = choice ;
    var THIS = this ;
    function wait_answer(data)
    {
         THIS.clock_stop() ;
         THIS.set_state('answer', data) ;
    }
    request('answer.json/[' + this.answer_sent + ']/' + this.nr_history,
             wait_answer) ;
} ;
Session.prototype.__grading__answer = function(data)
{
    data = JSON.parse(data) ;
    if ( data == "bug" )
        {
            this.question.innerHTML = "Il y a eu un problème technique. Réactualisez la page." ;
            return ;
        }
    var good = data[0] ;
    this.params.grade = Number(data[1]) ;
    var good_answer ;
    if ( this.keywords )
        {
            good_answer = JSON.stringify(this.answer) == JSON.stringify(good) ;
            if ( good_answer || data[3] )
                {
                    document.getElementById('answer').parentNode.className = 'keywords good_answer' ;
                    document.getElementById('answer').innerHTML +=
                        '<div class="points">+' + data[2].toFixed(2) + '</div>' ;
                    if (  data[3] && !good_answer )
                        {
                            // Display the BEST good answer
                            document.getElementById('good_answer').parentNode.className = 'keywords good_answer' ;
                            document.getElementById('good_answer').innerHTML = raw(this.get_phrase(good)) ;
                            good_answer = true;
                        }
                }
            else
                {
                    document.getElementById('answer').parentNode.className = 'keywords bad_answer' ;
                    document.getElementById('good_answer').parentNode.className = 'keywords good_answer' ;
                    document.getElementById('good_answer').innerHTML = raw(this.get_phrase(good)) ;
                }
        }
    else
        {
            good_answer = this.answer_sent == good ;
            var ol = this.question.getElementsByTagName('OL')[0] ;
            ol.childNodes[good].className += ' good_answer' ;
            if ( good_answer )
                ol.childNodes[this.answer_sent].innerHTML +=
                    '<div class="points">+' + data[2].toFixed(2) + '</div>' ;
            else
                ol.childNodes[this.answer_sent].className += ' bad_answer' ;
        }
    this.slot.style.background = (good_answer ? '#8F8' :  '#F88') ;
} ;
Session.prototype.set_state = function(state, data)
{
    var fct = this['__' + this.state + '__' + state] ;
    if ( fct === undefined )
        alert("Bug " + this.state + '→' + state + ' not allowed') ;
    else
        {
            var error = fct.bind(this)(data) ;
            if ( error )
                alert(error) ;
            else
                {
                    this.state = state ;
                    this.update_body_class() ;
                    this.html_update_title() ;
                }
        }
} ;

///////////////////////////////////////////////////////////////////////////////

Session.prototype.html_init = function()
{
    this.calendar = document.getElementById('calendar').firstChild ;
    this.message  = document.getElementById('message') ;
    this.question = document.getElementById('question') ;
    this.grade    = document.getElementById('grade') ;
    this.clock_icon= document.getElementById('show_the_clock') ;
    this.abcd_icon= document.getElementById('ABCD') ;
    this.clock    = document.getElementById('clock') ;
    this.clock_c  = this.clock.childNodes[2] ;
    this.body     = document.getElementsByTagName('BODY')[0] ;
    this.title    = document.getElementsByTagName('TITLE')[0] ;
    document.getElementsByTagName('TITLE')[0].textContent = this.params.name ;
    document.getElementById('style').textContent += '#calendar > TBODY > TR > TD { height: '
        +  this.calendar.childNodes[1].childNodes[0].offsetWidth / this.slot_x * this.slot_y
        +  'px ; }'
        + 'BODY { font-family: ' + this.params.fontFamily + '}'
        + 'BUTTON { font-family: ' + this.params.fontFamily + '}'
        ;
    this.ticket = window.location.search ;
    this.question_id = window.location.search.split('question_id=') ;
    history.replaceState('_a_', '_t_', window.location.toString().split('?')[0])
    this.html_update() ;
    // The state is added to the BODY class
    // 'message'  : message on screen
    // 'asking'   : waiting for the server to send the question
    // 'midnight' : no more question
    // 'question' : question on screen, time running
    // 'grading'  : time no more running, wait the grade
    // 'answer'   : answer on screen
    this.set_state('message') ;
} ;

Session.prototype.popup = function(url)
{
    var div = document.createElement('DIV') ;
    div.className = "popup" ;
    div.innerHTML += '<button class="close" onclick="S.popdown()">×</button><iframe src="'
        + url + this.ticket + '"></iframe>' ;
    this.body.appendChild(div) ;
} ;
Session.prototype.popdown = function(url)
{
    this.body.removeChild(this.body.lastChild) ;
} ;

Session.prototype.html = function()
{
    var t = [] ;
    t.push('<style id="style">#calendar .day TD { width: ' + (100/this.params.slot_x) + '%}</style>') ;
    t.push('<div id="all"><div class="title">') ;
    t.push(this.params.login) ;
    t.push(' <span id="grade"></span>') ;
    if ( this.params.see_secrets == 'True' )
        t.push(' <a href="javascript:S.popup(\'secrets/secrets/icon.png\')">S-Icons</a>') ;
    if ( this.params.see_secrets == 'True' )
        t.push(' <a href="javascript:S.popup(\'secrets/grades\')">S-Notes</a>') ;
    if ( this.params.see_icons == 'True' )
        t.push(' <a href="javascript:S.popup(\'icon.html\')">Icones</a>') ;
    if ( this.params.see_grades == 'True' )
        t.push(' <a href="javascript:S.popup(\'grades\')">Notes</a>') ;
    if ( this.params.see_activity == 'True' )
        t.push(' <a href="javascript:S.popup(\'activity\')">Activity</a>') ;
    if ( this.params.see_stats == 'True' ) {
        t.push(' <a href="javascript:S.popup(\'stats.html\')">Stats</a>') ;
        t.push(' <a href="javascript:S.popup(\'late.html\')">Late</a>') ;
        t.push(' <a href="javascript:S.popup(\'per_day.html\')">Days</a>') ;
        t.push(' <a href="javascript:S.popup(\'incomplete.html\')">Incomplete</a>') ;
    }
    t.push(' <span id="WeekEnd">Week<br>End</span>') ;
    t.push(' <span id="show_the_clock">⌚</span>') ;
    t.push(' <span id="ABCD">AB<br>CD</span>') ;
    t.push('</div><table id="calendar"><tbody>') ;

    t.push('<tr><td>Dim<td>Lun<td>Mar<td>Mer<td>Jeu<td>Ven<td>Sam</tr>') ;
    var day, d = 0, date = new Date() ;
    for(var i = this.real_start + 12*3600; // Fix daylight saving
        i < this.params.start_time + 86400 * this.params.days ;
        i += 86400, d++)
        {
            if ( d % 7 == 0 )
                t.push('<tr>') ;
            day = d - (this.start_day + 1) % 7 ;
            t.push('<td>')
            if ( this.debug )
                {
                    date.setTime(i*1000) ;
                    t.push('<i>' + day + '<br>'
                           + date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate()
                           + '</i>') ;
                }
            t.push('<table id="d' + day + '"><tbody>') ;
            for(var y=0; y<this.slot_y; y++)
                {
                    t.push('<tr>') ;
                    for(var x=0; x<this.slot_x; x++)
                        t.push('<td>') ;
                    t.push('</tr>') ;
                }
            t.push('</table>') ;
            if ( d % 7 == 6 )
                t.push('</tr>') ;
        }
    t.push('</table>') ;
    t.push('</div><div id="message">') ;
    t.push('</div><div id="question"></div>') ;
    t.push('<div id="clock"><div class="a"></div><div class="b"></div><div class="c"></div><div class="d"></div><div class="e"></div><div class="f"></div><div class="g"></div></div>') ;
    return t.join('') ;
} ;

Session.prototype.html_update_title = function()
{
    this.grade.innerHTML = this.params.grade.toFixed(1) + '/' + this.grade_max ;
    if ( this.debug )
        this.grade.innerHTML += '[' + this.state + ']' ;
} ;

Session.prototype.html_update = function()
{
    var days = this.calendar.getElementsByTagName('TABLE') ;
    var day, t, c ;
    var d = new Date() ;
    d.setHours(12) ;  // Fix daylight saving (25 hours in a day)
    d = d.getTime() / 1000 ;
    if ( this.debug )
        {
            this.debug_t = this.debug_t || 0 ;
            d += 86400 * this.debug_t ;
            this.debug_t++ ;
        }
    day = Math.ceil((d - this.params.start_time) / 86400) ;
    var nr = day ;
    for(var i in this.holidays)
        if ( this.holidays[i] < day )
            nr-- ;
    this.grade_max = nr * this.slot_x * this.slot_y ;
    for(var i = 0; i <days.length ; i++)
        {
            day = days[i] ;
            t = this.real_start + 86400 * i ;
            if ( t < this.params.start_time )
                c = 'before_start' ;
            else if ( t < d - this.params.nr_days_to_answer * 86400 )
                c = 'past' ;
            else if ( t < d - 86400 )
                c = 'fine' ;
            else if ( t < d )
                c = 'today' ;
            else
                c = 'future'
            if ( this.params.holidays.indexOf(i - 1 - this.start_day) != -1 )
                c += ' holidays' ;
            day.className = "day " + c ;
            day.slot = 0 ;
        }
    if ( this.params.start_time + 86400 * (this.params.days + this.params.nr_days_to_answer - 1)
         < d )
         this.ended = true ;
    var action, start ;
    t = 0 ;
    for(var i in this.history)
        {
            t += this.history[i][0] ;
            action = this.history[i][1] ;
            if ( action == 'start' )
                {
                    start = t
                    day = this.history[i][3] ;
                    continue ;
                }
            if ( action != 'good' && action != 'bad' )
                continue ;
            e = this.get_slot(day) ;
            if ( e ) // May be undefined if the number of questions per day is reduced
                if ( action == 'good' )
                    e.style.background = '#0' + '0123456789ABCDEF'.substr(16 - Math.log(2 + t - start), 1) + '0' ;
                else
                    e.style.background = '#F00' ;
        }
    this.html_update_title() ;
} ;


Session.prototype.get_slot = function(day, slot)
{
    var d = document.getElementById('d' + day) ;
    if ( ! d )
        return ;
    slot = slot || d.slot ;
    d.slot = Number(slot) + 1 ;
    return d.getElementsByTagName('TD')[slot] ;
} ;

Session.prototype.log = function(txt)
{
    if ( this.debug )
        console.log(txt) ;
} ;

Session.prototype.clock_stop = function(data)
{
    this.log('ClockStop') ;
    clearInterval(this.clock_id) ;
    this.clock_id = undefined ;
} ;

Session.prototype.clock_start = function(data)
{
    this.log('ClockStart dt=' + this.dt) ;
    this.clock_id = setInterval(this.clock_animate.bind(this), this.dt) ;
    this.clock_current = 0 ;
    this.clock_div = 0.9 ;
    this.clock_scale = Math.log(0.5) / Math.log(this.clock_div) ;
} ;

Session.prototype.clock_animate = function()
{
    //if ( this.debug )
    //    console.log('Clock ' + this.clock_current) ;
    this.clock_current += this.dt ;
    var t = this.clock_current / this.clock_time ;
    if ( t < 1 )
        t = 50 * t ;
    else
        t = 100 - 50 * this.clock_time / this.clock_current ;
    this.clock_c.style.top = t + '%' ;
} ;

Session.prototype.get_phrase = function(choices)
{
    var s = '' ;
    for(var i in choices)
        s += this.choices[choices[i]] ;
    return s ;
} ;

Session.prototype.update_answer = function()
{
    if ( ! this.keywords )
        return ;
    this.answer_text = this.get_phrase(this.answer) ;
    document.getElementById('answer').innerHTML = raw(this.answer_text)
        + '<span class="cursor"> </span>' ;
} ;

Session.prototype.update_body_class = function()
{
    this.body.className = (this.show_the_clock ? '' : 'clock_disabled ')
                        + (this.show_the_abcd ? '' : 'abcd_disabled ')
                        + (this.click_on_holiday ? '' : 'weekend_disabled ')
                        + this.state ;
} ;

var S ;

function doclick(event)
{
    S.log(event) ;
    var target = event.target ;
    if ( target.href || target.onclick )
        return ;
    if ( target.className == 'letter' )
        target = target.parentNode ;
    var table = target;
    while(table && table.tagName != 'TABLE')
        table = table.parentNode;
    if ( table && table.className.match('day') )
        {
            if ( table.className.match('today|fine') )
                if ( table.className.match('holidays') )
                    S.click_on_holiday = true;
                else
                    S.click_on_holiday = false;
        }
    switch( target.id )
    {
        case 'show_the_clock':
            S.show_the_clock = ! S.show_the_clock ;
            S.update_body_class() ;
            return ;
        case 'ABCD':
            S.show_the_abcd = ! S.show_the_abcd ;
            S.update_body_class() ;
            return ;
        case 'WeekEnd':
            S.click_on_holiday = ! S.click_on_holiday ;
            S.update_body_class() ;
            return ;
        }

    switch(S.state)
        {
            case 'midnight':
                S.set_state('initial') ;
                return ;
            case 'answer':
                if ( target.className == 'hate' )
                    {
                        var what ;
                        if ( target.parentNode.id == 'question' )
                            what = 'question' ;
                        else
                            what = target.parentNode.className.split(/[ _]/g)[1] ;
                        target.className += ' zoom' ;
                        request('hate_' + what + '/' + S.nr_history,
                                function() {
                                    target.style.opacity = 0;
                                    }) ;
                        return ;
                    }
                S.set_state('initial') ;
                return ;
            case 'initial':  S.set_state('message') ; return ;
            case 'message':
                if ( target.className == 'start' )
                    S.set_state('asking') ;
                else
                    S.set_state('initial') ;
                return ;
            case 'question':
                while ( target.className.length < 4 )
                    target = target.parentNode ; // For <m> tag
                switch( target.className.replace(/[0-9]*/g, '') )
                {
                    case 'send':
                        S.set_state('grading') ;
                        return ;
                    case 'backspace':
                        if ( S.answer.length )
                            {
                                S.answer.pop() ;
                                S.update_answer() ;
                            }
                        return ;
                    case 'choice':
                        var choice = Number(target.className.substr(6)) ;
                        if ( target.tagName == 'BUTTON' )
                            {
                                S.answer.push(choice) ;
                                S.update_answer() ;
                            }
                        else
                            S.set_state('grading', choice) ;
                        return ;
                }
                return ;
        }
}

function dokey(event)
{
    S.log(event) ;
    if ( S.state == 'question' )
        {
            if ( event.key.length == 1 && event.key != ' ' )
                {
                    var i = event.key.toLowerCase().charCodeAt(0)
                            - 'a'.charCodeAt(0) ;
                    if ( i >= 0 && i < S.nr_choices )
                        {
                            if ( S.keywords )
                                {
                                    S.answer.push(i) ;
                                    S.update_answer() ;
                                }
                            else
                                S.set_state('grading', i) ;
                        }
                    else if ( i == S.nr_choices && S.keywords )
                        S.set_state('grading') ;
                }
            else if ( event.key == 'Backspace' && S.keywords && S.answer.length )
                {
                    S.answer.pop() ;
                    S.update_answer() ;
                }
            else if ( event.key == ' ' && S.allowed_space !== false )
                {
                    S.answer.push(S.allowed_space) ;
                    S.update_answer() ;
                    event.stopPropagation() ;
                    event.preventDefault() ;
                }
            return ;
        }
    if ( event.key == 'Enter' )
        {
            new_state = {'initial':'message', 'message':'asking',
                         'answer':'initial', 'midnight':'initial'}[S.state] ;
            if ( new_state )
                S.set_state(new_state) ;
        }
}

function start(params)
{
    var d = new Date() ;
    d = d.getTime() / 1000 ;
    params = JSON.parse(params) ;
    if ( params[0].start_time > d )
        {
            document.getElementsByTagName('BODY')[0].innerHTML = "Attendez le "
                + params[0].start_date + " pour commencer le QCM."
            return ;
        }
    S = new Session(params) ;
    document.getElementsByTagName('BODY')[0].innerHTML = S.html() ;
    S.html_init() ;
    setInterval(S.html_update.bind(S), 60 * 1000) ; // Once per minute (for midnight)
    if ( S.question_id.length == 2 )
        S.set_state('asking') ;
}

request('session.json', start) ;
