#!/usr/bin/python3
# pylint: disable=invalid-name,missing-docstring,bad-whitespace,bare-except

import os
import sys
import random
import json
import exam_mcq

# sys.stderr = open('xxx.stderr', 'w') # Hide unecessary messages
random.seed(1)

def compare_to(filename, content):
    """Raise en error if the file does not contains the content"""
    filename = 'REGTESTS' + os.path.sep + filename

    try:
        with open(filename, 'r') as f:
            expected_content = f.read()
    except FileNotFoundError:
        with open(filename, 'w') as f:
            f.write(content)
        print(filename, "created")
        return

    if expected_content == content:
        return
    with open('xxx', 'w') as f:
        f.write(content)
    os.system("diff -u {} xxx".format(filename))
    raise ValueError("Regtests do not pass")

filenames = ("TESTS.animals", "TESTS.mythical", 'TESTS2.animals', 'TESTS.session')
print("Load {}".format(' '.join(filenames)))
q = exam_mcq.Questionnary()
q.regression_tests = True
q.load_files(*filenames)
compare_to('dump', str(q))

grps = '\n'.join(sorted('{}:({})'.format(k, ', '.join(sorted(qq.name for qq in v))) for k, v in q.groups().items()))
assert grps == ":(AAA, AnimalCover, AnimalEat, AnimalError, AnimalGood, AnimalQuestion, BBB, Keywords, Use Constant, Use Constant 2, WhichAnimalLegs, allgoods, any, goods, negatives)\neasy:(AnimalLegs)"

qq = q.questions['TESTS.animals.AnimalEat']
qq.select()
params = {k: v.get_str() for k, v in q.items['TESTS.session.default'].kargs.items()}
compare_to('state', json.dumps(qq.get_state(params), sort_keys=True))
assert qq.get_float('period', params) == 2

qq = q.questions['TESTS.animals.Keywords']
qq.select()
params = {k: v.get_str() for k, v in q.items['TESTS.session.default'].kargs.items()}
compare_to('state2', json.dumps(qq.get_state(params), sort_keys=True))

if q.stats() != '''46 : 16 questions, 6 alternatives, 24 items
90 ChoiceItem+ChoiceAttr''':
    raise ValueError("Bad stats : " + q.stats())

for a_filename, expected_error in (
        ("TESTS.bad0", "ValueError: TESTS.bad0.ANIMAL ['bug']\nbug not found"),
        ("TESTS.bad1", "use non existant attribute TESTS.animals.spider:badatr"),
        ("TESTS.bad2", "TESTS.animals.dragon not found"),
        ("TESTS.bad3", "assert 'good' in self.kargs"),
        ("TESTS.bad4", "exam_mcq.DuplicateIdentifier: TESTS.bad4.x"),
        ("TESTS.bad5", "assert self.kargs['good'].original"),
    ):
    print("Load {}".format(a_filename))
    error = q.load_files(a_filename)
    if expected_error not in error:
        print('-'*99)
        print(a_filename)
        print('-'*99)
        print(error)
        sys.exit(1)
    compare_to('dump', str(q))


qq = q.questions['TESTS.animals.AnimalEat']
qq.dump_html()
qq.select()
qq.dump_html()
qq = q.alternatives['TESTS.animals.ANIMAL']
qq.nice_dump([])
qq.select()
qq.nice_dump([])

q.load_files('TESTS.complex')
t = [exam_mcq.html_style]
for question_id in ('TESTS.complex.Complex1', 'TESTS.complex.Complex2',
                    'TESTS.animals.AnimalQuestion', 'TESTS.animals.negatives',
                    'TESTS.complex.Complex3'):
    qq = q.questions[question_id]
    qq.nice_dump(t)
    qq.select()
    qq.nice_dump(t)
    t.append('<br>')
    t.append(repr(qq.get_state({})))
    t.append('<br>')

compare_to('attr_replace.html', '\n'.join(t))

qq = q.questions['TESTS.complex.NoChoice']
try:
    qq.select()
    raise ValueError("Do not detect: Always identical choices for TESTS.complex.NoChoice []")
except:
    del q.questions['TESTS.complex.NoChoice']

qq = q.questions['TESTS.complex.NoChoice2']
try:
    qq.select()
    raise ValueError("Do not detect No choice possible for TESTS.complex.NoChoice2 []")
except:
    del q.questions['TESTS.complex.NoChoice2']

qq = q.questions['TESTS.animals.allgoods']
qq.select()
result = qq.get_state({})
if result != ({'time': 0.0,
               'time_scale': 0.0,
               'question': 'Answer «snailcover»',
               'choices': ['cover', 'snail'],
               'collapse_spaces': 0,
               'keywords': True
              }, [[1, 0], [0, 1]], 'coversnail'):
    raise ValueError("Unexpected: " + repr(result))

qq = q.questions['TESTS.complex.Z']
qq.select()
s = qq.get_state({})[0]
if s['question'] != '«DA»[-1] must be equal to «CA»[-1]':
    raise ValueError("Negation: " + repr(s))

for i in range(10):
    print("Generate exam {}".format(i))
    random.seed(i)
    t = []
    for qq in q.questions.values():
        qq.select()
        t.append(qq.get_string())
        for choice in qq.choices:
            t.append(str(choice.parent))
    compare_to('questions{}'.format(i), '\n'.join(t))

print("Regtests are fine")
