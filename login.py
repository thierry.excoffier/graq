# pylint: disable=invalid-name,missing-docstring,bad-whitespace,too-few-public-methods

"""
Question may be modified at any time.
So it is not possible to reboot and give again the same problem.

In case of reboot, question not answered are canceled.

"""

import ast
import time
import datetime
import hashlib
import json
import collections
import codecs
import asyncio
import os
import re
from typing import Dict, DefaultDict, List, Union, Iterable, Set, Any, Tuple, IO, Optional # pylint: disable=unused-import
import PIL.Image
import exam_mcq

Action = List[Any]

def strftime(t):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(t))

time_names, time_values = tuple(zip(*[
    ['En cours'   , 0],
    ['1 Minute'   , 60],
    ['5 Minutes'  , 60*5],
    ['10 Minutes' , 60*10],
    ['15 Minutes' , 60*15],
    ['30 Minutes' , 60*30],
    ['1 Heure'    , 60*60],
    ['2 Heures'   , 60*60*2],
    ['4 Heures'   , 60*60*4],
    ['8 Heures'   , 60*60*8],
    ['16 Heures'  , 60*60*16],
    ['1 Jour'     , 60*60*24],
    ['2 Jours'    , 60*60*24*2],
    ['4 Jours'    , 60*60*24*4],
    ['1 Semaine'  , 60*60*24*7],
    ['2 Semaines' , 60*60*24*7*2],
    ['3 Semaines' , 60*60*24*7*3],
    ['4 Semaines' , 60*60*24*7*4],
    ['8 Semaines' , 60*60*24*7*8],
    ['12 Semaines', 60*60*24*7*12],
    ['...'        , 1e11]
    ]))

class QuestionStat: # pylint: disable=too-many-instance-attributes
    def __init__(self, question, params) -> None:
        self.key          = question.key
        self.time         = question.get_float('time'        , params)
        self.first_day    = question.get_float('first_day'   , params)
        self.period       = question.get_float('period'      , params)
        self.max_increase = question.get_float('max_increase', params)
        self.if_bad       = question.get_float('if_bad'      , params)
        self.if_good      = question.get_float('if_good'     , params)
        self.next_day     = self.first_day
        # The integer is the hash of the good answer
        self.viewed       = set() # type: Set[int]
        self.answers      = []
    def __str__(self) -> str: # pragma: no cover
        return '{:40} t={:5.1f} fd={:5.1f} p={:5.1f} nd={:5.1f}'.format(
            self.key, self.time, self.first_day, self.period, self.next_day)

class Login: # pylint: disable=too-many-instance-attributes,too-many-public-methods,too-many-statements
    def __init__(self, login:str, t:int, protocol, rewrite=False) -> None:
        login = login.replace('/', '_')
        self.login = login
        self.OPTIONS = protocol.OPTIONS
        self.DEFAULTS = protocol.DEFAULTS
        self.QUESTIONNARY = protocol.QUESTIONNARY
        self.history = [] # type: List[Any]
        self.question_history = None # type: Optional[int]
        self.question_time    = -1   # type: int
        self.question_state   = {}   # type: Dict[str,Any]
        self.question_day     = -1   # type: int
        self.question_name    = '?!' # type: str
        self.question_good    = []   # type: List[List[int]]
        self.last_time = 0
        self.grade = 0
        self.days = collections.defaultdict(lambda: (0,0,0)) # type: DefaultDict[int,Tuple[int,int]]
        self.init_params()
        self.stats = {q.key: QuestionStat(q, self.params)
                      for q in self.QUESTIONNARY.questions.values()}
        self.slots_per_day = int(self.params['slot_x']) * int(self.params['slot_y'])
        self.see_secrets  = self.params['see_secrets' ] == 'True'
        self.see_icons    = self.params['see_icons'   ] == 'True'
        self.see_grades   = self.params['see_grades'  ] == 'True'
        self.see_activity = self.params['see_activity'] == 'True'
        self.see_stats    = self.params['see_stats'   ] == 'True'
        self.see_log      = self.params['see_log'     ] == 'True'
        self.allow_exit   = self.params['allow_exit'  ] == 'True'
        self.grade_max    = int(self.params['grade_max'])
        self.nr_days_lag  = int(self.params['nr_days_lag'])
        self.params['login'] = self.login
        self.params['debug'] = self.params['debug'] == 'True'
        self.debug = self.params['debug']
        self.time_scale = float(self.params['time_scale'])
        self.percent_bad_to_remove = int(self.params['percent_bad_to_remove'])
        self.logname = "{}/{}.log".format(self.OPTIONS.logs, login)
        if rewrite:
            with open(self.logname + '~', "w") as w:
                with open(self.logname, "r") as r:
                    for text_line in r:
                        line = ast.literal_eval(text_line)
                        if len(line) == 4 and line[1] == 'start':
                            line.append(0)
                        w.write(repr(line) + '\n')
            os.rename(self.logname + '~', self.logname)
        try:
            with open(self.logname, "r") as f:
                for line in f:
                    self.eval(ast.literal_eval(line))
            self.newuser = False
        except FileNotFoundError:
            self.time = t
            self.newuser = True
        self.params['grade'] = self.grade
        if self.newuser:
            self.log('newuser')
        # Do not count holidays
        self.time = int(time.time())
        today = self.get_current_day()
        for day in self.params['holidays']:
            if day > today:
                break
            self.nr_days_lag += 1

        #if self.debug:
        #    print('\n'.join(sorted(str(i) for i in self.stats.values())))
        if False:
            print('*'*99)
            self.time = int(time.time())
            print('holidays=', self.holidays)
            print('current day=', self.get_current_day())
            print('max_grade=', self.max_grade())

    def init_params(self) -> None:
        self.params = params = dict(self.DEFAULTS)
        user_defaults = self.QUESTIONNARY.items.get(
            self.OPTIONS.session + '.' + self.login, None)
        if user_defaults:
            for k, v in user_defaults.kargs.items():
                params[k] = v.get_str()
        self.session_days = int(self.params['days'])
        params['start_time_tuple'] = time_tuple = time.strptime(params['start_date'], '%Y-%m-%d')
        self.datetime_start = datetime.datetime(*time_tuple[:3])
        params['start_time'] = start = time.mktime(time_tuple)
        params['replacements'] = ast.literal_eval(params['replacements'])
        weekend = ast.literal_eval(params['weekend'])
        holidays = set()
        start -= 12*3600 # To fix daylight saving
        for date in re.split('[ \t\n]+', params['holidays']):
            if not date:
                continue
            if '/' in date:
                date, nb = date.split('/')
                nb = int(nb)
            else:
                nb = 1
            date = int(time.mktime(time.strptime(date, '%Y-%m-%d')) - start) // 86400
            for i in range(date, date+nb):
                holidays.add(i)
        if weekend: # Do not work some days every week
            for i in range(self.session_days):
                if (i + time_tuple.tm_wday) % 7 in weekend:
                    holidays.add(i)
        params['holidays'] = sorted(holidays)
        self.holidays = holidays

    def get_grade(self) -> float:
        return self.grade_max * self.grade / (self.max_grade() or 1)

    def get_grade_best(self) -> float:
        vals = []
        for day in range(min(self.get_current_day() + 1, self.session_days)):
            if day not in self.holidays:
                vals.append(self.days[day][2])
        vals.sort()
        to_remove = len(vals) * self.percent_bad_to_remove // 100
        if to_remove:
            del vals[:to_remove]
        if vals:
            return self.grade_max * sum(vals) / len(vals) / self.slots_per_day
        else:
            return -1

    def get_question_asked(self):
        return set(stat
                   for stat in self.stats.values()
                   if stat.first_day != stat.next_day
                  )

    def get_nr_questions(self) -> float:
        return len(self.get_question_asked())

    def max_grade(self) -> int:
        nr = day = min(self.get_current_day() + 1, self.session_days)
        for d in self.params['holidays']:
            if d < day:
                nr -= 1
        return nr * self.slots_per_day

    def askable_questions(self) -> List[QuestionStat]:
        day = self.get_current_day() + 0.5 - self.nr_days_lag
        return [i for i in self.stats.values() if i.first_day < day]

    def select(self) -> Optional[exam_mcq.Question]:
        """
        Each question has a period in days.
        On good answer: increase the period.
        On bad answer: decrease the period.

        Initial day: the start day.
        On answer add the period to the day.

        Choose the question with the minimum number.
        Do not ask question when the start day is in the future.
        """
        qs = self.askable_questions()
        if qs:
            qs.sort(key = lambda i: i.next_day)
            return self.QUESTIONNARY.questions[qs[0].key]
        return None # No questions (before exam beginning?)

    def eval(self, action:Action) -> None:
        # if self.debug:
        #     print("====", action)
        self.last_time += action[0]
        if action[1] == 'start':
            self.question_history = len(self.history)
            self.question_time    = self.last_time
            self.question_day     = action[3]
            self.question_name    = action[2]
            self.question_last_history = self.question_history
            # Save the hash key of the expected answer for this question
            self.stats[action[2]].viewed.add(action[4])
        elif action[1] == 'cancel':
            self.question_history = None
        elif action[1] in ('good', 'bad'):
            self.question_history = None
            q = self.stats[self.question_name]
            q.answers.append(action[1] == 'good')
            qtime = q.time * self.time_scale
            nr_good, nr_bad, grade = self.days[self.question_day]
            self.QUESTIONNARY.questions[self.question_name].update_stats(
                action[1] == 'good', self.last_time - self.question_time)
            if action[1] == 'good':
                dt = max(self.last_time - self.question_time, 1) # Not 0
                action.append(dt)
                if self.question_day not in self.holidays:
                    # Grade change only if not for the holidays
                    if dt <= qtime:
                        add_grade = 1
                    else:
                        add_grade = qtime / dt # pragma: no cover
                else:
                    add_grade = 0
                self.grade += add_grade
                q.period *= min( (1 + qtime / (dt+1)) * q.if_good,  q.max_increase)
                self.days[self.question_day] = (nr_good+1, nr_bad, grade + add_grade)
            else:
                q.period /= q.if_bad
                self.days[self.question_day] = (nr_good, nr_bad+1, grade)
            q.next_day += q.period
        elif action[1] == 'hate_question':
            self.QUESTIONNARY.questions[self.question_name].hate_question += 1
        elif action[1] == 'hate_bad':
            self.QUESTIONNARY.questions[self.question_name].hate_bad += 1
        elif action[1] == 'hate_good':
            self.QUESTIONNARY.questions[self.question_name].hate_good += 1

        self.history.append(action)

    def log(self, *args) -> None:
        action = [self.time - self.last_time, *args]
        with open(self.logname, "a") as f:
            f.write(repr(action) + '\n')
        self.eval(action)

    async def session(self, f:codecs.StreamWriter):
        self.log('load')
        self.params['grade'] = self.grade
        f.write(json.dumps([self.params, self.history]))
        return

    def get_current_day(self) -> int:
        now = datetime.datetime(*time.localtime(self.time)[:3])
        return (now - self.datetime_start).days

    def get_first_day(self) -> int:
        return self.get_current_day() - int(self.params['nr_days_to_answer']) + 1

    def get_first_slot(self, holiday:bool) -> Tuple[Optional[int], Optional[int]]:
        day = self.get_first_day()
        for _ in range(int(self.params['nr_days_to_answer'])):
            is_an_holiday = not holiday and day in self.holidays
            if not is_an_holiday and day >= 0 and self.days[day][0] + self.days[day][1] < self.slots_per_day:
                return day, self.days[day][0] + self.days[day][1]
            day += 1
        return None, None

    def last_activity(self) -> int:
        if self.question_state and self.question_history is not None:
            return 0
        last_activity = time.time() - self.last_time
        for i, t in enumerate(time_values):
            if last_activity < t:
                return i
        return 99 # pragma: no cover

    async def start(self, f:codecs.StreamWriter, holiday, question_id=None) -> None:
        if not self.question_state and self.question_history is not None:
            # The server has been rebooted. Free the slot
            self.log('cancel')
        if self.question_history is None:
            if question_id is None:
                q = self.select()
            else:
                # The debug mode in Stats page allows to try any question
                q = self.QUESTIONNARY.questions[question_id]
            if q is None:
                # Exam not started ?
                f.write('{}')
                return
            for _ in range(3): # Tries 3 times to have a never saw good answer
                q.select()
                self.question_state, self.question_good, good = q.get_state(self.params)
                # Store the hash of the good answer to not use too much memory
                h = int(hashlib.blake2b(good.encode('utf8'), digest_size=3).hexdigest(), 16) # pylint: disable=no-member
                if h not in self.stats[q.key].viewed:
                    break
            self.question_state['day'], self.question_state['slot'] = self.get_first_slot(holiday)
            self.question_state['history'] = len(self.history)
            if self.debug:
                self.question_state['key'] = q.key
            if self.question_state['day'] is None:
                # No more free slots in the day
                f.write('{}')
                return
            self.log('start', q.key, self.question_state['day'], h)
        f.write(json.dumps(self.question_state))

    async def answer(self, f:codecs.StreamWriter, user_choice:str, user_history:str) -> None:
        choice = ast.literal_eval(user_choice)
        history = int(user_history)
        if not self.question_state and self.question_history is not None:
            # The server has been rebooted. Free the slot
            self.log('cancel')
            f.write('"bug"')
            return
        if history != self.question_history:
            # The student played with multiple window
            f.write('"bug"')
            return
        before = self.grade
        if choice in self.question_good:
            self.log('good')
        else:
            self.log('bad')
        f.write(json.dumps([self.question_good[0], self.grade, self.grade - before,
                            choice in self.question_good]))

    def secret(self, action) -> str:
        return self.login + '/' + action + '/' + hashlib.md5((
            self.login
            + '.' + action
            + '.' + str(self.history[0][0])
            + '.' + str(os.stat(self.logname).st_ino)
            + self.params['secret']
            ).encode("utf-8")).hexdigest()

    async def icon(self, f:IO[bytes]):
        today = self.get_current_day()
        if today < 0:
            return # pragma: no cover
        width = min(today + 1, self.session_days)
        img = PIL.Image.new("RGB", (width, self.slots_per_day))
        pixels = [(255,128,128)] * (width * self.slots_per_day)
        try:
            for day in range(width+1):
                nr_good, nr_bad, _grade = self.days[day]
                d = day + (self.slots_per_day-1) * width
                for i in range(0, nr_good):
                    pixels[d - i*width] = (0, 255, 0)
                for i in range(nr_good, nr_bad+nr_good):
                    pixels[d - i*width] = (255, 0, 0)
                if day in self.holidays:
                    for i in range(nr_bad+nr_good, self.slots_per_day):
                        pixels[d - i*width] = (255, 255, 255)
        except IndexError:  # pragma: no cover
            pass
        img.putdata(pixels)
        img.save(f, 'PNG')

    def new_ticket(self, t:int, ip:str, browser:str) -> None:
        self.time = t
        self.log('ticket', ip, browser)

    async def display_stats_questions(self, f, students) -> None:
        f.write('''
        <script>
        var direction = 1 ;
        function sort(event)
        {
            var td = event.target ;
            var table = td.parentNode.parentNode ;
            var lines = [] ;
            for(var i in table.rows)
            {
                if (i == 0)
                    continue ;
                var s = [] ;
                for(var j in table.rows[i].cells)
                        {
                            var v = table.rows[i].cells[j].innerHTML ;
                                s.push(isNaN(v) ? v : Number(v)) ;
                        }
                lines.push(s) ;
            }
            var col = td.cellIndex ;
            lines.sort(
                function(a, b) { return a[col] < b[col] ? -direction : (a[col] > b[col] ? direction : 0) ; }
                ) ;
            for(var i in table.rows)
            {
                if (i == 0)
                    continue ;
                for(var j in table.rows[i].cells)
                    table.rows[i].cells[j].innerHTML = lines[i-1][j] ;
            }
            direction = -direction ;
        }
        function jump(url, search)
        {
            search = window.location.search + (search ? '&'+search : '') ;
            window.location = window.location.origin
                + window.location.pathname.replace(RegExp('/[^/]*$'), '/' + url)
                + search ;
        }
        </script>
        <table>
        ''')
        f.write('<tr onclick="sort(event)">'
                '<td>%BAD<td>%&lt;t<td>%&gt;t<td>%GOOD<td>%&lt;t<td>%&gt;t<td>t'
                '<td>#answ<td>😠Quest<td>😠good<td>😠bad<td>😠total<td>NextDay'
                '<td>dt<td>DT<td>FirstDay<td>Question</tr>\n')
        stats = [] # type: List[List[str]]
        current_day = (self.time - self.QUESTIONNARY.session_start) / 86400
        empty = {} # type: Dict[str,str]
        for k, q in self.QUESTIONNARY.questions.items():
            s = q.get_stats(self.DEFAULTS)
            next_day = [] # type: List[float]
            period = [] # type: List[float]
            for student in students:
                if k in student.stats:
                    next_day.append(student.stats[k].next_day)
                    period.append(student.stats[k].period)
            if next_day:
                s.append(str(int(sum(next_day) / len(next_day) - current_day)))
                s.append('{:.1f}'.format(sum(period) / len(period)))
            else:
                s.append('?') # pragma: no cover
                s.append('?') # pragma: no cover
            s.append(str(self.QUESTIONNARY.questions[k].get_float('period',empty)))
            s.append(str(self.QUESTIONNARY.questions[k].get_float('first_day',empty)))
            s.append('<a href="javascript:jump(\'question.html/{0}\')">{0}</a>'
                     .format(k))
            s.append('<a href="javascript:jump(\'\', \'question_id={}\')">GO!</a>'
                     .format(k))
            stats.append(s)
            await asyncio.sleep(0)
        stats.sort(key = lambda x: x[7])
        for stat in stats:
            f.write('<tr><td>' + '<td>'.join(stat) + '</tr>\n')
        f.write('</table>')

    async def display_stats_ip(self, f, students, session, times) -> None:
        f.write('Students by IP:')
        ips = collections.defaultdict(set) # type: Dict[str, set]
        for student in students:
            t = 0
            for h in student.history:
                t += h[0]
                if h[1] == 'ticket':
                    ips[h[2]].add(student.login)
                elif h[1] in ('bad', 'good'):
                    times.append((t, student.login))
            await asyncio.sleep(0)
        f.write('<pre>')
        f.write('\n'.join(k + ' ' + ' '.join(self.link(session.get_login(s))
                                             for s in v)
                          for k, v in sorted(ips.items())
                          if len(v) > 1))
        f.write('</pre>')

    async def display_stats_same_time(self, f, session, times) -> None:
        # pylint: disable=too-many-locals,too-many-branches
        f.write('Same time:')
        f.write('<pre>')
        times.sort()
        await asyncio.sleep(0)

        session_cut = 3600 # Maximum time without interaction in a session
        session_linked = 600 # Maximum time between linked sessions
        session_students = 40 # Number of nodes to display
        session_edges = 4 # Increase to display more edges

        # Search session (one hour without working)

        sessions = [] # type: List[Tuple[str,int,int]]
        current_sessions = {} # type: Dict[str,int]
        for t, student in times:
            current_session = current_sessions.get(student, None)
            if current_session is None:
                current_sessions[student] = len(sessions)
                sessions.append((student, t, t))
            else:
                _, start, end = sessions[current_session]
                if t - end > session_cut: # pragma: no cover
                    current_sessions[student] = len(sessions)
                    sessions.append((student, t, t))
                else:
                    sessions[current_session] = (student, start, t)
        await asyncio.sleep(0)

        # Search student answering after another

        same_time = collections.defaultdict(lambda: collections.defaultdict(int)) \
            # type: Dict[str,Dict[str,int]]
        for i, (student1, _, end1) in enumerate(sessions):
            t_max = end1 + session_linked
            student1_dict = same_time[student1]
            for j in range(i+1, len(sessions)):
                student2, start2, _ = sessions[j]
                if start2 > t_max:
                    break # pragma: no cover
                student1_dict[student2] += 1
            await asyncio.sleep(0)

        # The greater: the more been copied
        level = collections.defaultdict(int) # type: Dict[str,int]
        cheaters = []
        for student1, others in same_time.items():
            for student2, nr_times in others.items():
                cheaters.append((nr_times, student1, student2))
                level[student1] += nr_times
                level[student2] -= nr_times
            await asyncio.sleep(0)
        cheaters.sort(reverse=True)
        await asyncio.sleep(0)
        to_display = [] # type: List[str]
        for _, student1, student2 in cheaters:
            if student1 in to_display:
                if student2 not in to_display:
                    to_display.insert(to_display.index(student1)+1, student2) # pragma: no cover
            else:
                if student2 in to_display:
                    to_display.insert(to_display.index(student2)+1, student1)
                else:
                    to_display.append(student1)
                    to_display.append(student2)
            if len(to_display) > session_students:
                break # pragma: no cover
        level_max = max(level.values())
        level_min = min(level.values())
        for student, l in level.items():
            if student in to_display:
                continue
            if l > level_max * 0.7 or l < level_min * 0.7: # pragma: no cover
                to_display.append(student)

        f.write('</pre><table><tr><td>')
        await asyncio.sleep(0)
        for student in to_display:
            f.write('<td>{}'.format(self.link(session.get_login(student)).replace('">', '"><br>')))
        f.write('</tr>')
        maxi = cheaters[0][0]
        min_level = min(level.values())
        max_level = max(level.values())
        dot = []
        await asyncio.sleep(0)

        # DOT nodes

        for student1 in to_display:
            session.get_login(student1).time = self.time
            if level[student1] > 0:
                x = 1 - level[student1] / max_level
                color = "{0}{0}FF{0}{0}".format("0123456789ABCDEFF"[int(x*16)])
            else:
                x = 1 - level[student1] / min_level
                color = "FF{0}{0}{0}{0}".format("0123456789ABCDEFF"[int(x*16)])
            dot.append(
                '{0} [fillcolor="#{1}" label="{0}\\n{3}\\n{2}/20"]'
                .format(student1, color, int(session.get_login(student1).get_grade()),
                        level[student1]))

        # DOT edges

        for student1 in to_display:
            f.write('<tr><td>{}'.format(self.link(session.get_login(student1))))
            for student2 in to_display:
                if student1 == student2:
                    f.write('<td>')
                else:
                    n = same_time.get(student1, {}).get(student2, 0)
                    n2 = same_time.get(student2, {}).get(student1, 0)
                    if (n+n2) < maxi // session_edges:
                        f.write('<td><span style="color:#DDD">{}'.format(n)) # pragma: no cover
                    else:
                        if student1 >= student2:
                            dot.append(
                                '{} -- {} [ headlabel="{}" taillabel="{}" penwidth={}];'.format(
                                    student2, student1, n, n2,
                                    int(16 * max(n, n2) / maxi)
                                    ))
                        f.write('<td>{}'.format(n))
            f.write('</tr>')
            await asyncio.sleep(0)
        f.write('</table>')
        dot_str = ('graph {overlap = false;\nedge [ color="#CCCC00" ] node [ style="filled" ]\n'
                   + '\n'.join(dot) + '\n}').encode('utf8')
        with open("xxx.dot", "wb") as g:
            g.write(dot_str)
        for displayer in ('neato',):
            f.write(displayer)
            f.write("""
            Legend:
            <ul>
            <li> A session stops when there is {} minutes without work.
            <li> Two sessions are linked if the second starts less than {} minutes
                after the first session ends.
            <li> The linked sessions are called First and Second.
            <li> The {} nodes are students, the label indicates:
                <ul>
                <li> Student ID
                <li> Number of First sessions minus the number of Second sessions
                    taking account all the students, even not in the graph.
                <li> The grade of the student
                </ul>
            <li> The edge label near the node indicates the number of First session,
                the other edge label indicates the number of Second session.
            <li> The edge widths is proportionnal to the number of linked sessions.
            <li> The node color is green if there are more First session
                than Second ones.
            <li> Only edges whose labels sum ≥{} are displayed.
            </ul>
            """.format(session_cut//60, session_linked//60, session_students,
                       maxi // session_edges))
            proc = await asyncio.create_subprocess_shell(
                displayer + ' -T svg',
                stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
            stdout, _ = await proc.communicate(dot_str)
            f.write(stdout.decode())


    async def display_stats_next_day(self, f, students) -> None:
        day = [0]*999
        for student in students:
            next_day = int(min(i.next_day for i in student.stats.values()))
            day[next_day] += 1
            await asyncio.sleep(0)
        t = list(enumerate(day))
        # Never starts with 0 because of the debug login
        # while t[0][1] == 0:
        #     t.pop(0)
        while t[-1][1] == 0:
            t.pop()
        current_day = self.get_current_day()
        f.write('<p>Number of students per next_day. 0 is today')
        f.write('<pre>')
        for i, nr in t:
            f.write("{:3} : {:3} {}\n".format(i - current_day, nr, '*'*nr))
            await asyncio.sleep(0)
        f.write('</pre>')

    async def display_stats(self, f, session) -> None:
        f.write('<pre>')
        f.write(self.QUESTIONNARY.stats())
        f.write('</pre>')
        students = await session.students()
        await asyncio.sleep(0)
        await self.display_stats_questions(f, students)
        await asyncio.sleep(0)
        f.write('Questions defined per day from start day\n')
        f.write('<pre>')
        f.write(self.QUESTIONNARY.stats_day())
        f.write('</pre>')
        times = [] # type: List[Tuple[float, str]]
        await self.display_stats_ip(f, students, session, times)
        await asyncio.sleep(0)
        await self.display_stats_same_time(f, session, times)
        await self.display_stats_next_day(f, students)

    async def display_late(self, f, session) -> None:
        students = await session.students()
        await asyncio.sleep(0)
        today = self.get_current_day()
        f.write(f"day={today} time={time.ctime(self.time)}<br>")
        nr_days = 14
        days = [[] for i in range(nr_days+1)] # type: List[List[str]]
        for student in students:
            for i in range(nr_days):
                nr_good, nr_bad, _grade = student.days[today-i]
                if nr_good + nr_bad == self.slots_per_day:
                    days[i].append(student.login)
                    break
            else:
                days[nr_days].append(student.login)
        f.write("Last fully completed day :<table border>")
        t = 86400 * (int(time.time()) // 86400) + 43200
        for i in range(nr_days+1):
            f.write('<tr><td>{}<td>{}</tr>'.format(
                time.strftime('%Y-%m-%d<br>%A', time.localtime(t)),
                ' '.join(days[i])))
            t -= 86400
        f.write('</table>')

    async def answers_per_day(self, f, session) -> None:
        students = await session.students()
        await asyncio.sleep(0)
        nr_days = self.get_current_day() + 1
        days = [[0, 0, 0] for i in range(nr_days+1)] # type: List[List[int]]
        weeks = [set() for i in range(nr_days+1)] # type: List[Set[str]]
        for student in students:
            for i in range(nr_days):
                nr_good, nr_bad, _grade = student.days[i]
                days[i][0] += nr_good
                days[i][1] += nr_bad
                if nr_good + nr_bad:
                    days[i][2] += 1
                    weeks[i//7].add(student.login)
        await asyncio.sleep(0)
        t = self.params['start_time'] + 43200
        f.write('<p>Number of working student per week<br>')
        for i in range(nr_days // 7): # pragma: no cover
            f.write('{} {}<br>'.format(
                time.strftime('%Y-%m-%d', time.localtime(t)),
                len(weeks[i])))
            t += 86400 * 7
        await asyncio.sleep(0)
        f.write('''
        <style>
        .per_days { border-spacing: 0px ; border: 1px solid #888 }
        .per_days TD { border: 1px solid #888 }
        .per_days TD:nth-child(3) { width:10em }
        .per_days TD:nth-child(3) DIV DIV { display: inline-block }
        .per_days TD:nth-child(3) DIV DIV:first-child { background:#0F0 }
        .per_days TD:nth-child(3) DIV DIV:last-child { background:#F00 }
        </style>
        <table class="per_days" border>
        <tr><td>Day<td>Nr students<td>Good+Bad</tr>
        ''')
        t = self.params['start_time'] + 43200
        m = max(days, key=lambda x: x[0] + x[1])
        mm = (m[0] + m[1]) / 100
        for i in range(nr_days):
            f.write('''<tr><td>{}<td>{}<td><div><div style="width:{}%"> </div><div style="width:{}%"> </div></div></tr>
                '''.format(
                    time.strftime('%Y-%m-%d %A', time.localtime(t)),
                    days[i][2],
                    int(days[i][0] / mm),
                    int(days[i][1] / mm),
                    ))
            t += 86400
        f.write('</table>')

    async def display_per_day(self, f, session) -> None:
        students = await session.students()
        await asyncio.sleep(0)
        today = self.get_current_day()
        nr_days = 14
        days = [[] for i in range(nr_days+1)] # type: List[List[str]]
        working_students = set()
        for student in students:
            for i in range(nr_days):
                nr_good, nr_bad, _grade = student.days[today-i]
                if nr_good + nr_bad == self.slots_per_day:
                    working_students.add(student)
                    break
        for student in working_students:
            for i in range(nr_days):
                nr_good, nr_bad, _grade = student.days[today-i]
                if nr_good + nr_bad < self.slots_per_day:
                    days[i].append('{}:{}'.format(student.login, nr_good + nr_bad))
        f.write("Last incompletes days :<table border>")
        t = 86400 * (int(time.time()) // 86400) + 43200
        for i in range(nr_days+1):
            f.write('<tr><td>{}<td>{}</tr>'.format(
                time.strftime('%Y-%m-%d<br>%A', time.localtime(t)),
                ' '.join(days[i])))
            t -= 84600
        f.write('</table>')

    async def display_log(self, f) -> None:
        t = 0
        s = []
        question = None
        for h in self.history:
            t += h[0]
            action = h[1]
            if action == 'start':
                question = [strftime(t), h[2],
                            str(int(self.stats[h[2]].time * self.time_scale)) + "s"]
                s.append(question)
            else:
                if question is None:
                    question = [strftime(t), ""]
                    s.append(question)
                question.append(action)
                if action == 'good':
                    question.append(f"{h[-1]}s")
        f.write('<br>'.join(' '.join(i) for i in s))

    def link(self, student:"Login") -> str:
        if self.see_log:
            return ('<a href="?ticket=' + student.secret('log.html') + '"><img src="_?ticket='
                    + student.secret('icon.png') + '"/>'
                    + student.login + '</a>')
        return student.login

    async def display(self, f, path:str, session) -> None:
        # pylint: disable=too-many-branches,too-many-locals,too-many-statements
        self.time = int(time.time())
        if path == '/session.json':
            await self.session(f)
        elif path == '/start.json/0':
            await self.start(f, False)
        elif path == '/start.json/1':
            await self.start(f, True)
        elif path.startswith('/answer.json/'):
            await self.answer(f, *path.split('/')[2:])
        elif path == '/grades' and self.see_grades:
            for student in await session.students():
                student.time = self.time
                askable = student.askable_questions()
                if askable:
                    nr = "{:.2f}".format(student.get_nr_questions() / len(askable))
                else:
                    nr = "-1"
                f.write('{}\t{:3.2f}\t{}\t{:3.2f}\n'.format(
                    student.login, student.get_grade(), nr, student.get_grade_best()))
        elif path.startswith('/secrets/') and self.see_secrets:
            which = path.split('/', 2)[2]
            for student in await session.students():
                f.write('{}\t{}\n'.format(student.login, student.secret(which)))
        elif path ==  '/icon.png':
            await self.icon(f)
        elif path ==  '/activity' and self.see_activity:
            a = collections.defaultdict(list) # type: Dict[int, List[Login]]
            for student in await session.students():
                a[student.last_activity()].append(student)
            nrs = 0
            for i, label in enumerate(time_names):
                nrs += len(a[i])
                f.write('{:>12} [{:4d}]===> {}\n'.format(
                    label, nrs, ' '.join(student.login for student in a[i])))
        elif path ==  '/exit-unsafe' and self.allow_exit:
            asyncio.get_event_loop().stop()
            return
        elif path ==  '/exit-safe' and self.allow_exit:
            while True:
                student_working = False
                for student in await session.students():
                    if student.last_activity() == 0:
                        thinking_time = time.time() - student.question_time
                        f.write('{} is answering since {} sec\n'.format(
                            student.login, thinking_time))
                        if thinking_time < 3600:
                            student_working = True
                            break
                if not student_working:
                    asyncio.get_event_loop().stop()
                    return
                await asyncio.sleep(1)

        elif path == '/stats.html' and self.see_stats:
            await self.display_stats(f, session)
        elif path == '/late.html' and self.see_stats:
            await self.display_late(f, session)
        elif path == '/per_day.html' and self.see_stats:
            await self.answers_per_day(f, session)
        elif path == '/incomplete.html' and self.see_stats:
            await self.display_per_day(f, session)
        elif path.startswith('/question.html/') and self.see_stats:
            q = self.QUESTIONNARY.questions[path.split('/',2)[2]]
            q.select()
            f.write(q.dump_html())
            f.write(repr(q.get_state({})))
        elif path ==  '/icon.html' and self.see_icons:
            f.write('''<style>
            DIV { display: inline-block ; width: 10em }
            IMG { transition: transform 1s ; transform-origin: top left ;image-rendering: pixelated}
            DIV:hover IMG { transform: translate(0em, 1.5em) scale(10, 10) ; }
            </style>''')
            for student in sorted(await session.students(), key=lambda s: s.login):
                f.write('<div>' + self.link(student) + '</div>\n')
        elif path.startswith(('/hate_question/', '/hate_bad/', '/hate_good/')):
            _, action, history = path.split('/')
            if int(history) == self.question_last_history:
                self.log(action)
                f.write('1')
        elif path == '/log.html':
            await self.display_log(f)
        elif path.startswith('/start.json/') and self.debug:
            await self.start(f, holiday=True, question_id=path.split('/')[2])
        else:
            f.write('BUG "{}"'.format(path))
