#!/usr/bin/python3
# pylint: disable=invalid-name,missing-docstring,bad-whitespace

import time
import os
import sys
import asyncio
import ast
import aiohttp
import yarl

port       = 57263
url        = 'http://127.0.0.1:' + str(port) + '/'
start_date = time.strftime("%Y-%m-%d", time.localtime(time.time()-86400))
holidays   = (time.strftime("%Y-%m-%d", time.localtime(time.time()))
              + time.strftime("  %Y-%m-%d/2 ", time.localtime(time.time()+10*86400)))

def start_server(erase, option=''):
    print("Start server erase={} option={}".format(erase, option))
    if erase:
        os.system('rm -rf LOGS-REGTESTS 2>/dev/null ; mkdir LOGS-REGTESTS')
    logname = 'xxx.{}'.format(start_server.log_id)

    # Create bad logfile needing rewrite
    with open('LOGS-REGTESTS/debug.log', 'w') as f:
        f.write("[0, 'start', 'START.start.A010', 0]\n")

    start_server.log_id += 1
    os.system("""
    python3-coverage run -a ./server.py --logs LOGS-REGTESTS \
                --ip 127.0.0.1 \
                --url {} \
                --port {} \
                --start_date {} \
                --holidays '{}' \
                --cas '' \
                --session 'TESTS/session.py' \
                {} START/start.py >{} \
                &
    """.format(url, port, start_date, holidays, option, logname))
    return logname
start_server.log_id = 0

async def load(an_url, hide_exception=False, binary=False):
    try:
        async with aiohttp.ClientSession() as http_session:
            async with http_session.get(yarl.URL(an_url, encoded=True) ) as response:
                if binary:
                    data = await response.read()
                else:
                    data = await response.text()
    except:
        if hide_exception:
            return None
        raise
    return data

errors = []
def begin(txt):
    errors.append(txt)
    print("{:40}".format(txt), end='')
def end(test):
    if test:
        errors.pop()
        print("OK")
    else:
        print("BAD")

async def wait_server_start():
    begin('Reading home page')
    for _ in range(100):
        d = await load(url, hide_exception=True)
        if d:
            break
        await asyncio.sleep(0.1)
    else:
        end(False)
    end('<!DOCTYPE html>' in d)

async def wait_server_stop():
    for _ in range(10):
        d = await load(url, hide_exception=True)
        if d is None:
            break
        await asyncio.sleep(0.1)
    else:
        return "Not stopped"


async def test_static_files():
    for name, expect in (('mcq.css@4543', '#calendar'),
                         ('mcq.js@34353', 'Session.prototype')
                        ):
        begin('Reading ' + name)
        d = await load(url + name)
        end(expect in d)

async def test_secret():
    begin('Reading secrets')
    d = await load(url + 'secrets/secrets/secrets?ticket=debug')
    end(d.startswith('debug\tdebug/'))

    begin('Use secret')
    d2 = await load(url + '_?ticket=' + d.split('\t')[1].strip())
    end(d2.startswith('debug\tdebug/'))

    begin('Use bad secret')
    d3 = await load(url + '_?ticket=' + d.split('\t')[1].strip() + "?")
    end(d3 == '')

async def session(): # pylint: disable=too-many-statements
    begin('Reading session.json')
    d = await load(url + 'session.json?ticket=regtests')
    end(start_date in d
        and '"slot_x": "2"' in d
        and '"slot_y": "1"' in d
        and ', "newuser"]' in d
        and ', "ticket",' in d
        and '[0, "load"]' in d
       )
    begin('Reading start.json')
    d = await load(url + 'start.json?ticket=regtests')
    end('Donnez-moi un point' in d
        and '"slot": 0' in d
        and '"history": 3' in d
        and '"day": 0' in d
       )
    good = 1 if ', "Donnez-moi un point' in d else 0
    begin('Reading1 answer.json')
    d = await load(url + 'answer.json/[' + str(good) + ']/3?ticket=regtests')
    end(d == '[[{}], 1, 1, true]'.format(good))

    begin('Reading start.json')
    d = await load(url + 'start.json?ticket=regtests')
    end('Vous cliquez dessus afin' in d
        and '"slot": 1' in d
        and '"history": 5' in d
        and '"day": 0' in d
       )

    begin('Reading start.json same question')
    d = await load(url + 'start.json?ticket=regtests')
    end('Vous cliquez dessus afin' in d)

    begin('Look activity')
    d = await load(url + 'activity?ticket=debug')
    end('En cours [   1]===> regtests' in d
        and 'Minute [   2]===> debug' in d
       )

    begin('Reading2 answer.json')
    d = await load(url + 'answer.json/[0]/5?ticket=regtests')
    if d == '[[0], 2, 1, true]':
        end(True)
        grade = 2
    else:
        end(d.endswith(', 1, 0, false]'))
        grade = 1

    begin('Look activity after')
    d = await load(url + 'activity?ticket=debug')
    end('Minute [   2]===> debug regtests' in d
        or 'Minute [   2]===> regtests debug' in d
       )

    begin('Reading colors')
    d = await load(url + 'start.json?ticket=regtests')
    d = await load(url + 'answer.json/[0]/7?ticket=regtests')
    end(d.startswith('['))

    begin('Reading start.json no questions: day 0')
    d = await load(url + 'start.json?ticket=regtests')
    end('"day": 1' in d)

    begin('Reading start.json on holiday')
    d = await load(url + 'start.json?ticket=regtests')
    end("Combien y-a-t-il de jours dans la semaine" in d)

    begin('Reading3 answer.json')
    choices = d.split('"choices": ')[1].split(']')[0] + ']'
    choices = ast.literal_eval(choices)
    good = choices.index("7")
    d = await load(url + 'answer.json/[' + str(good) + ']/9?ticket=regtests')
    end(d == '[[{}], {}, 0, true]'.format(good, grade)) # Not points++ on holidays

    begin('Hate')
    d = await load(url + 'hate_question/9?ticket=regtests')
    d = await load(url + 'hate_good/9?ticket=regtests')
    d = await load(url + 'hate_bad/9?ticket=regtests')
    end(d == '1')

    begin('Look grades')
    d = await load(url + 'grades?ticket=debug')
    if grade == 2:
        end('regtests\t20.00' in d)
    else:
        end('regtests\t10.00' in d)

    begin('Answering a question yet answered')
    d = await load(url + 'answer.json/[0]/5?ticket=regtests')
    end(d == '"bug"')



    begin('Reading bad URL')
    d = await load(url + '.?ticket=regtests')
    end(d == 'BUG "/."')

    begin('Reading all icon page')
    d = await load(url + 'icon.html?ticket=debug')
    end('<img src="_?ticket=regtests/icon.png/' in d)

    begin('Reading my log')
    d = await load(url + 'log.html?ticket=regtests')
    end('START.start.A010' in d)

    begin('Many answers (for same time detection)')
    for student in ('s1', 's2'):
        await load(url + 'session.json?ticket=' + student)
    ok = True
    for answer in range(10):
        for student in ('s1', 's2'):
            await load(url + 'start.json?ticket=' + student)
            d = await load(url + 'answer.json/[0]/{}?ticket={}'.format(
                3 + 2*answer, student))
            if not d.startswith('[['):
                ok = False
    end(ok)

jobs = []


async def get_stats():
    begin('Look stats day')
    d = await load(url + 'stats.html?ticket=debug')
    end('Questions defined per day from start day\n<pre>0	15	15' in d
        and '<a href' in d
       )

    begin('Look stats day (!see_log)')
    d = await load(url + 'stats.html?ticket=stats')
    end('Questions defined per day from start day\n<pre>0	15	15' in d
        and '<a href="?' not in d
       )

    begin('Debug question')
    d = await load(url + 'question.html/START.start.A100?ticket=stats')
    end('<div><b>ArgString A100</b><br>' in d)

async def no_more_slots():
    begin('No more slots')
    ok = True
    for answer in range(20):
        d = await load(url + 'start.json?ticket=s1')
        if d == '{}':
            break
        d = await load(url + 'answer.json/[0]/{}?ticket=s1'.format(24 + 2*answer))
        if not d.startswith('[['):
            ok = False
    end(ok)

async def overload(username):
    for _ in range(12):
        d = await load(url + 'start.json?ticket=' + username)
        history = d.split('"history": ')[1].split('}')[0].split(',')[0]
        d = await load(url + 'answer.json/[0]/' + history + '?ticket=' + username)
        assert d.startswith('[[')
    d = await load(url + 'icon.png?ticket=' + username, binary=True)
    assert b'PNG' in d
    jobs.append(username)

async def run_overload():
    start = time.time()
    n = 40
    for i in range(n):
        asyncio.ensure_future(overload('xxx{}'.format(i)))
    while len(jobs) != n:
        await asyncio.sleep(0.1)
    print('{} sessions of 12 questions done in {:.2}s'.format(n, time.time() - start))
    d = await load(url + 'grades?ticket=debug')
    print(d.replace('\t', ' ').replace('\n', ' '))


async def restart():
    begin('Ask question, but do not answer')
    d = await load(url + 'start.json?ticket=yyy1')
    end('"history": 2' in d)
    begin('Ask question, but do not answer')
    d = await load(url + 'start.json?ticket=yyy2')
    end('"history": 2' in d)

    await load(url + 'exit-unsafe?ticket=debug')
    await wait_server_stop()
    start_server(erase=False)
    await wait_server_start()

    begin('Try to answer after restart')
    d = await load(url + 'answer.json/[0]/2?ticket=yyy1')
    end(d == '"bug"')
    begin('Try to ask again after restart')
    d = await load(url + 'start.json?ticket=yyy2')
    end('"history": 5' in d and 'une minute' in d) # 'cancel' in logs

    begin('Try to answer again after restart')
    d = await load(url + 'answer.json/[0]/5?ticket=yyy2')
    end(d.endswith(']'))

async def head():
    begin('Test HEAD')
    try:
        async with aiohttp.ClientSession() as http_session:
            async with http_session.head(yarl.URL(url, encoded=True) ) as response:
                await response.read()
        end(False)
    except aiohttp.client_exceptions.ServerDisconnectedError:
        end(True)

async def stop():
    await load(url + 'exit-safe?ticket=debug')
    print("exit_safe done", end=' ')

async def exit_safe():
    begin('Ask question, but do not answer')
    d = await load(url + 'start.json?ticket=yyy3')
    end('"history": 2' in d)

    asyncio.ensure_future(stop())

    begin('Try to safely stop (must not stop)')
    a = await wait_server_stop()
    end(a == 'Not stopped')

    begin('Answer')
    d = await load(url + 'answer.json/[0]/2?ticket=yyy3')
    end(d.endswith(']'))

    begin('Wait stop')
    a = await wait_server_stop()
    end(a is None)

    await asyncio.sleep(0.1)

async def start_before_beginning():
    begin('Start before')
    d = await load(url + 'start.json?ticket=future')
    end(d == '{}')


async def choose_question():
    begin('Chhose question')
    d = await load(url + 'start.json/START.start.A010?ticket=debug')
    end('Je ne veux pas de point' in d)


async def weekend():
    begin('Weekend')
    d = await load(url + 'session.json?ticket=weekend')
    h = ast.literal_eval(d.split('"holidays": ')[1].split(']')[0] + ']')
    end('"weekend": "[5,6]"' in d  and  len(h) >= 34)

async def display_late():
    begin('Late')
    d = await load(url + 'late.html?ticket=debug')
    end('</table>' in d)

async def per_day():
    begin('Per day')
    d = await load(url + 'per_day.html?ticket=debug')
    end('</table>' in d)

async def incomplete():
    begin('Incomplete')
    d = await load(url + 'incomplete.html?ticket=debug')
    end('</table>' in d)

async def tests():
    await wait_server_start()
    await head()
    await test_static_files()
    await test_secret()
    await session()
    await choose_question()
    await restart()
    await start_before_beginning()
    await get_stats()
    await no_more_slots()
    await get_stats()
    await weekend()
    await display_late()
    await per_day()
    await incomplete()
    await run_overload()
    await exit_safe()
    start_server(erase=False)
    await wait_server_start()
    logname = start_server(erase=False, option="--replace --profile")
    with open(logname, 'r') as f:
        while True:
            c = f.read()
            if "Stopped safely, now replace it" in c:
                print("Start the new server")
                break
            await asyncio.sleep(0.1)
    await wait_server_start()

async def profile():
    await wait_server_start()
    await run_overload()
    asyncio.ensure_future(stop())
    await wait_server_stop()
    print()
    print("Profile in xxx.0")

if 'profile' in sys.argv:
    start_server(erase=True, option='--profile')
    asyncio.get_event_loop().run_until_complete(profile())
else:
    start_server(erase=True)
    try:
        asyncio.get_event_loop().run_until_complete(tests())
    finally:
        try:
            asyncio.get_event_loop().run_until_complete(load(url + 'exit-unsafe?ticket=debug'))
        except: # pylint: disable=bare-except
            print("Not running!")
            sys.exit(2)

    if errors:
        print("ERROR " * 10)
        print("ERROR " * 10)
        print('\n'.join(errors))
        print("ERROR " * 10)
        print("ERROR " * 10)
        sys.exit(1)
