#!/usr/bin/python3
# pylint: disable=invalid-name,missing-docstring,bad-whitespace

import asyncio
from urllib.parse import quote
import codecs
import time
import socket
import sys
import os
import argparse
from typing import List, Dict, Any, Tuple, IO, Optional
import aiohttp
import exam_mcq
from login import Login

codec = codecs.getwriter("utf-8")

def create_questionnary(session:str, files:List[str], start_date='', holidays=''
                       ) -> Tuple[exam_mcq.Questionnary,Dict[str,Any]]:
    questionnary = exam_mcq.Questionnary()
    error = questionnary.load_files(session, *files)
    if error:
        raise ValueError(error) # pragma: no cover
    default = questionnary.items[session + '.default']
    params_default = {
        'holidays': '',
        'weekend': '[5,6]',
        'days': 99999,
        'nr_days_to_answer': 3,
        'slot_x': 4,
        'slot_y': 3,
        'see_grades': False,
        'see_secrets': False,
        'see_activity': False,
        'see_stats': False,
        'see_icons': False,
        'see_log': False,
        'allow_exit': False,
        'debug': False,
        'grade_max': 20,
        'secret': '',
        'replacements': '',
        'fontFamily': 'serif',
        'time_scale': 1,
        'nr_days_lag': 0,
        'percent_bad_to_remove': 0 # % of bad day grade to remove (3rd grade export column)
    } # type: Dict[str,Any]
    params_default.update({
        k: str(v)
        for k, v in default.kargs.items()})
    if start_date:
        params_default['start_date'] = start_date
    if holidays:
        params_default['holidays'] = holidays
    questionnary.slots_per_day = int(params_default['slot_x']) * int(params_default['slot_y'])
    questionnary.session_start = int(time.mktime(
        time.strptime(params_default['start_date'], '%Y-%m-%d')))

    return questionnary, params_default

def parse_options() -> Tuple[argparse.Namespace, exam_mcq.Questionnary, Dict[str,Any]]:
    port = '8080'
    parser = argparse.ArgumentParser(description="Grading Exercises Web Server")
    parser.add_argument("--port",
                        help="Socket port of the server",
                        default=port)
    parser.add_argument("--ip",
                        help="IP on wich open the port",
                        default=socket.gethostname())
    parser.add_argument("--url",
                        help="Public URL of the server",
                        default='http://{}:{}/'.format(
                            socket.gethostname(), port))
    parser.add_argument("--cas",
                        help="CAS server for authentication",
                        default='https://cas.univ-lyon1.fr/cas')
    parser.add_argument("--session",
                        help="The Item defining the session parameters",
                        default='TESTS.session')
    parser.add_argument("--logs",
                        help="The directory name storing user data",
                        default='LOGS')
    parser.add_argument("--start_date",
                        help="Override the session start date",
                        default='')
    parser.add_argument("--holidays",
                        help="A space separated list of holiday dates",
                        default='')
    parser.add_argument("--replace",
                        help="Replace a running server",
                        action="store_true",
                        default=False)
    parser.add_argument("--profile",
                        help="Profile the server (for developpers)",
                        action="store_true",
                        default=False)
    parser.add_argument('filenames',
                        nargs='*',
                        help='The filenames containing the questions',
                        default=['START.start'])
    options = parser.parse_args()
    options.session = options.session.replace(os.path.sep, '.')
    if options.session.endswith('.py'):
        options.session = options.session[:-3]
    s = sys.argv[0] + ' \\'
    for opt in getattr(parser, '_actions'):
        value = getattr(options, opt.dest, False)
        if value is False:
            continue
        s += "\n    " + '/'.join(str(o) for o in opt.option_strings)
        if value is True:
            s += ' \\'
            continue
        if isinstance(value, list):
            v = ' '.join(value)
        else:
            v = repr(value)
        s += " " + v + " \\"
    log(s[:-2])
    questionnary, params_default = create_questionnary(
        options.session, options.filenames, options.start_date, options.holidays)
    return options, questionnary, params_default

def read_file(filename:str) -> str:
    with open(filename, 'r') as f:
        return f.read()

def read_binary_file(filename:str) -> bytes:
    with open(filename, 'rb') as f:
        return f.read()

mimetypes = {
    'mcq.css'    : 'text/css',
    'mcq.js'     : 'text/javascript',
    'favicon.ico': 'image/png',
    'documentation.html': 'text/html',
    }

def log(*args) -> None:
    print(*args, flush=True)

class NoLogin(Login):
    login = '?'
    def __init__(self): # pylint: disable=super-init-not-called
        pass
no_login = NoLogin()

class Session: # pylint: disable=too-many-instance-attributes
    logins = {} # type: Dict[str,Login]
    valid_ticket = False # type: Optional[bool]
    replace_path = ''

    def __init__(self, ticket:str, ip:str, browser:str, protocol:"HTTPProtocol") -> None:
        self.ip = ip
        self.current_ip = ip
        self.browser = browser
        self.time = time.time()
        self.protocol = protocol
        self.ticket = ticket
        self.login = no_login # type: Login
        self.log("New session")

    def log(self, txt):
        print("{}\t{}\t{}\t{}\t{}".format(
            time.strftime("%Y-%m-%d %H:%M:%S"),
            self.login.login,
            self.ticket.split("-")[1] if '-' in self.ticket else self.ticket,
            self.current_ip, txt),
              flush=True)

    async def activate_cas(self) -> None: # pragma: no cover
        # self.log("Activate CAS " + self.ticket)
        async with aiohttp.ClientSession() as session:
            url = ('{}/validate?service={}&ticket={}'.format(
                self.protocol.cas, self.protocol.url, quote(self.ticket)))
            # self.log("url validate " + url)
            async with session.get(url) as response:
                data = await response.text()
                # self.log("answer " + data)
        lines = data.split('\n')
        if lines[0] == 'yes':
            self.validate(lines[1].lower())
            return
        self.protocol.redirection()

    async def activate(self) -> None:
        # self.log("TICKET " + self.ticket)
        if '/' in self.ticket:
            login, action_secret = self.ticket.split('/', 1)
            if os.path.exists('{}/{}.log'.format(self.protocol.OPTIONS.logs, login)):
                student = self.get_login(login)
                action, dummy_secret = action_secret.rsplit('/', 1)
                if student.secret(action) == self.ticket:
                    self.valid_ticket = True
                    self.replace_path = action
                    self.login = student
                else:
                    self.valid_ticket = None
                    self.log("Bad secret")
            return
        if not self.protocol.cas:
            # Not authentication (debugging purpose)
            self.validate(self.ticket)
            return
        await self.activate_cas() # pragma: no cover

    def get_login(self, login:str) -> Login:
        if login not in self.logins:
            try:
                self.logins[login] = Login(login, int(self.time), self.protocol)
            except: # pylint: disable=bare-except
                print("Log file reading failed: rewrite it", file=sys.stderr)
                self.logins[login] = Login(login, int(self.time), self.protocol,
                                           rewrite=True)
        return self.logins[login]

    def validate(self, login:str):
        self.login = self.get_login(login)
        self.login.new_ticket(int(self.time), self.ip, self.browser)
        self.valid_ticket = True

    async def display(self, transport:IO[bytes], path:str) -> None:
        self.log("Display " + path)
        for _ in range(10):
            if self.valid_ticket:
                break
            if self.valid_ticket is False: # Not sleep on invalid ticket
                await asyncio.sleep(0.1) # pragma: no cover
        else:
            transport.write(b"HTTP/1.0 200 OK\n"
                            b"Content-Type: application/json; charset=UTF-8\n"
                            b"\n")
            transport.close()
            return
        if self.replace_path:
            # The ticket contains the action and the secret
            path = '/' + self.replace_path
        if path.endswith('.png') and path.count('/') == 1:
            transport.write(b"HTTP/1.0 200 OK\n"
                            b"Content-Type: image/png\n"
                            b'Cache-Control: max-age=86400\r\n'
                            b"\n")
            await self.login.display(transport, path, self)
        else:
            if '.json' in path:
                mime = b'application/json'
            elif '.html' in path:
                mime = b'text/html'
            else:
                mime = b'text/plain'
            transport.write(b"HTTP/1.0 200 OK\n"
                            + b"Content-Type: " + mime + b"; charset=UTF-8\n"
                            + b"\n")
            f = codec(transport)
            await self.login.display(f, path, self)
        transport.close()

    async def students(self) -> List[Login]:
        t = []
        for login in os.listdir(self.protocol.OPTIONS.logs):
            t.append(self.get_login(login[:-4]))
            await asyncio.sleep(0)
        return t

class HTTPProtocol(asyncio.Protocol):
    sessions = {} # type: Dict[str, Session]

    @classmethod
    def finish_init(cls):
        cls.OPTIONS, cls.QUESTIONNARY, cls.DEFAULTS = parse_options()
        cls.cas = cls.OPTIONS.cas
        cls.url = cls.OPTIONS.url.rstrip('/')

    def connection_made(self, transport) -> None:
        self.transport = transport # pylint: disable=attribute-defined-outside-init

    def redirection(self) -> None:
        self.transport.write(b"HTTP/1.0 307 Temporary Redirect\n")
        if self.cas:
            self.transport.write("Location: {}/login?service={}\n".format(
                self.cas, self.url).encode()) # pragma: no cover
        else:
            self.transport.write("Location: {}?ticket=debug\n"
                                 .format(self.url).encode())
        self.transport.write(b"\n")
        self.transport.close()

    def send_file(self, filename:str) -> None:
        log("Static file", filename)
        content = read_binary_file(filename)
        self.transport.write(("HTTP/1.0 200 OK\r\n"
                              + "Content-Type: {}\r\n".format(mimetypes[filename])
                              + "Content-Length: {}\r\n".format(len(content))
                              + 'Cache-Control: max-age=8640000\r\n'
                              + "\n").encode())
        self.transport.write(content)
        self.transport.close()

    def send_home(self) -> None:
        self.transport.write(read_file('mcq.html').format(
            int(os.path.getmtime('mcq.css')),
            int(os.path.getmtime('mcq.js')),
            ).encode('utf-8'))
        self.transport.close()

    def data_received(self, raw:bytes) -> None: # pylint: disable=arguments-differ
        # pylint: disable=too-many-branches
        data = raw.decode(errors='replace')
        if '\r\n' in data:
            lines = data.split('\r\n')
        else:
            lines = data.split('\n') # pragma: no cover
        if not lines[0].startswith('GET '):
            log("HACKER", lines[0])
            self.transport.close()
            return
        request = lines[0].split(' ')[1]
        splited = request.split('?ticket=')
        if len(splited) > 1:
            ticket = splited[1].split('&')[0]
        else:
            ticket = ''
        url = request.split('?')[0]
        if url == '/' and  not ticket:
            self.redirection()
            self.transport.close()
            return
        if ticket:
            browser = ''
            ip = ''
            host = ''
            for line in lines:
                line = line.lower()
                if line.startswith('user-agent: '):
                    browser = line.split(': ', 1)[1]
                if line.startswith('x-forwarded-for: '):
                    ip = line.split(' ', 1)[1].split(',')[0] # pragma: no cover
                if line.startswith('host: '):
                    host = line.split(' ', 1)[1]
            if not ip:
                ip = host
            if ticket in self.sessions:
                if self.sessions[ticket].browser != browser:
                    if '/' not in ticket:
                        log("BrowserChanged", request)
                        self.redirection()
                        self.transport.close()
                        return
                self.sessions[ticket].current_ip = ip
            else:
                self.sessions[ticket] = Session(ticket, ip, browser, self)
                asyncio.ensure_future(self.sessions[ticket].activate())
        if url in ('/', '/mcq.html'):
            self.send_home()
            return
        url_without_timestamp = url.split('@')[0][1:]
        if url_without_timestamp in mimetypes:
            self.send_file(url_without_timestamp)
            return
        if not ticket: # pragma: no cover
            log("Redirection", request)
            self.redirection()
            self.transport.close()
            return
        # The display may wait for session activation
        asyncio.ensure_future(self.sessions[ticket].display(self.transport, url))

    @classmethod
    async def start(cls):
        log(cls.url)
        loop = asyncio.get_event_loop()
        server = await loop.create_server(HTTPProtocol, cls.OPTIONS.ip, cls.OPTIONS.port)
        await server.wait_closed()

    @classmethod
    async def stop(cls):
        log("Restart: wait the other server stop")
        secret = Session('', '', '', cls).get_login('debug').secret('exit-safe')
        url = 'http://' + cls.OPTIONS.ip + ':' + cls.OPTIONS.port + '/_?ticket=' + secret
        log(url)
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(url, timeout=86400) as response:
                    await response.read()
            log("Stopped safely, now replace it")
            sys.stdout.flush()
        except aiohttp.client_exceptions.ClientConnectorError: # pragma: no cover
            log("No server were running: nothing stopped")

if __name__ == '__main__':
    if not os.path.exists('LOGS'):
        os.mkdir('LOGS')
    HTTPProtocol.finish_init()

    if HTTPProtocol.OPTIONS.replace:
        asyncio.get_event_loop().run_until_complete(HTTPProtocol.stop())

    if HTTPProtocol.OPTIONS.profile:
        import cProfile
        profile = cProfile.Profile()
        profile.enable()

    asyncio.ensure_future(HTTPProtocol.start())
    asyncio.get_event_loop().run_forever()

    if HTTPProtocol.OPTIONS.profile:
        profile.disable()
        profile.print_stats(1)
