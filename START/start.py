

Question("A010",
    question = """
        Vous avez **une minute** pour lire avec attention ce texte et répondre.
        Vous avez donc le temps.

        Un chronomètre est affiché sur la partie gauche.
        Arrivé à la moitié, une minute pour cette question,
        vous commencez à perdre des points.

        La réponse à cette question, comme toutes les autres
        permettra de calculer votre note de contrôle continu.
        Donc ne répondez pas n'importe quoi.

        Cliquez sur le bon choix qui suit,
        et quand vous aurez gagné votre point, cliquez à nouveau
        pour faire disparaître cette fenêtre.
        """,
    good = "Donnez-moi un point s'il vous plaît",
    bad = ('Je ne veux pas de point, je sais que cela va faire baisser ma note',
          ),
    time = 60,
    first_day = 0.01,
    period = 999,
    )

Question("A015",
    question = """
        Après avoir répondu des petits 😠 apparaissent à coté
        de la question, de la bonne et de la mauvaise réponse.
        Vous cliquez dessus afin d'indiquer que vous n'êtes pas d'accord.

        Vous ne cliquez donc **pas** dessus pour :
        """,
    good = (
        "Voir une petite animation",
        "Indiquer que vous êtes fatigué",
        "Indiquer que vous ne voulez plus que l'on vous pose la question",
        ),
    bad = ('Indiquer que la question est incompréhensible ou contient des erreurs',
           "Indiquer que votre réponse a été refusée alors qu'elle est bonne",
           "Indiquer que vous n'êtes pas d'accord avec la bonne réponse",
          ),
    time = 60,
    first_day = 0.015,
    period = 999,
    )

Item('verts' , quoi="bonnes réponses")
Item('rouges', quoi="mauvaises réponses")
Item('roses' , quoi="questions auxquelles vous n'avez pas répondus à temps")
Item('bleus' , quoi="questions auxquelles vous pouvez répondre")
Item('gris'  , quoi="questions pas encore accessibles")
Alternative('COULEUR', 'verts', 'rouges', 'roses', 'bleus', 'gris')

Question("A020",
    question = """
        Votre note actuelle est affichée en haut.

        Voici les couleurs possible pour les carrés :

          * {verts} : {:quoi} ;
          * {rouges} : {:quoi} ;
          * {roses} : {:quoi} ;
          * {bleus} : {:quoi} ;
          * {gris} : {:quoi}.

        Que représentent les carrés {COULEUR} ?
        """,
    good = "{COULEUR:quoi}",
    bad = ("{!COULEUR:quoi}",) * 4,
    time = 60,
    first_day = 0.02,
    period = 999,
    )

Question("A030",
    question = """
        Ce qui est affiché est un calendrier avec une semaine par ligne.
        Les jours de la semaine forment les colonnes.
        Les carrés sont regroupés par jour.

        Quand il n'y a plus de carrés {bleus} il faut attendre minuit
        pour avoir de nouvelles questions.

        Combien y-a-t-il de jours dans la semaine ?
        """,
    good = "7",
    bad = (4,5, 8,9),
    time = 60,
    first_day = 0.03,
    period = 999,
    )

Question("A040",
    question = """
        On haut de la page, vous trouverez votre login ainsi que votre note actuelle.
        Tant que vous pouvez répondre à des questions, faites-le,
        vous gagnez ainsi des points et augmenterez votre note.

        Vous pouvez encore répondre à des questions maintenant s'il reste des carrés :
        """,
    good = "{bleus}",
    bad = ("{!COULEUR}",) * 4,
    time = 60,
    first_day = 0.04,
    period = 999,
    )


Question("A050",
    question = """
        Si vous faites une erreur en répondant à une question,
        la bonne réponse sera indiquée.
        La question vous sera proposée à nouveau dans un futur proche.

        Si vous répondez correctement, elle sera reproposée dans
        une futur plus éloigné.
        Plus vous répondez vite, plus ce futur sera éloigné.

        Est-il possible que l'on vous propose la même question plusieurs fois ?
        """,
    good = "Oui",
    bad = ("Non",),
    time = 60,
    first_day = 0.05,
    period = 999,
    )

Question("A060",
    question = """
        Si vous répondez à ce questionnaire en utilisant un clavier.
        Alors appuyez sur la touche «Entrée» pour lancer les questions
        et sur la lettre correspondant à votre choix pour envoyer
        votre réponse.

        Après avoir répondu, vous pouvez utiliser la touche «Entrée»
        pour faire disparaître la question.

        Pour répondre aux questions vous pouvez frapper la lettre indiquée
        à gauche du choix que vous désirez faire.

        Quelles sont les touches clavier qu'il est possible d'utiliser
        pour répondre à cette question ?
        """,
    good = "A, B et C",
    bad = ("A et B", "A, B, C et D"),
    time = 60,
    first_day = 0.06,
    period = 999,
    )

Question("A065",
    question = """
        Vous n'utilisez pas le clavier pour répondre et
        les lettres ABCD vous gênent.

        Maintenant : cliquez sur l'icone ABCD en haut, qu'est-ce qui se passe sur l'écran ?
        """,
    good = ("Une croix apparaît sur ABCD", "Les lettres disparaissent instantanément"),
    bad = ("L'icone ABCD disparaît",
           "L'icone ABCD devient rouge",
           "L'icone ABCD devient grisée",
          ),
    time = 60,
    first_day = 0.065,
    period = 999,
    )


Question("A070",
    question = """
        Si jamais vous vous trompez de réponse, ce que vous avez choisi
        passe en rouge et la bonne réponse passe en vert.

        Est-ce qu'il y a du rouge quand vous répondez bien ?
        """,
    good = "Non",
    bad = ("Oui",),
    time = 60,
    first_day = 0.07,
    period = 999,
    )

Question("A080",
    question = """
        La barre de gauche avec le temps qui avance vous angoisse, vous stresse ?
        Vous pouvez la cacher en cliquant sur l'icone ⌚
        qui est en haut à droite.

        Maintenant : cliquez sur ⌚, qu'est-ce qui se passe sur l'écran ?
        """,
    good = ("Une croix apparaît sur ⌚", "La barre de gauche apparaît/disparaît instantanément"),
    bad = ("L'icone ⌚ disparaît",
           "L'icone ⌚ devient rouge",
           "L'icone ⌚ devient grisée",
          ),
    time = 60,
    first_day = 0.08,
    period = 999,
    )

Question("A090",
    question = """
        Concernant la note affichée, elle n'est pas définitive,
        elle peut être recalculée de manière rétroactive si

          * vous obtenez un tiers-temps ;
          * le temps de réponse à la question avait été mal évalué.
          
        En conséquence, la note affichée :
        """,
    good = "peut être ajustée à tout moment",
    bad = ("ne va pas changer toute seule",),
    time = 60,
    first_day = 0.09,
    period = 999,
    )

Question("A100",
    question = """
        Vous pouvez aussi travailler le samedi et le dimanche.
        Mais si vous ne pouvez pas, vous pouvez rattraper
        votre retard le lundi.

        Si vous n'avez pas répondu aux questions du vendredi,
        il faudra impérativement le faire pendant le WE
        sinon le lundi il risque d'être trop tard.

        Quelle est l'affirmation **fausse** ?
        """,
    good = "Le samedi et le dimanche ne sont pas affichés",
    bad = ("La première colonne c'est le dimanche",
            "La dernière colonne c'est le samedi",
            "Le samedi et le dimanche sont grisés",
            ),
    time = 60,
    first_day = 0.10,
    period = 999,
    )

Question("A105",
    question = """
        Si vous cliquez sur «Week End» dans le menu en haut à droite,
        vous pouvez alors faire les questions d'entraînement du
        weekend et des vacances qui ne rapportent pas de points.

        Quelle est l'affirmation **fausse** ?
        """,
    good = "Il n'est pas possible de répondre aux questions du weekend",
    bad = ("Je peux répondre aux questions du weekend",
           "Les questions du weekend ne rapportent pas de points",
           "Je peux continuer ce QCM pendant les vacances",
          ),
    time = 60,
    first_day = 0.105,
    period = 999,
    )

Question("A110",
    question = """
        Certaines questions nécessitent d'écrire la réponse
        en cliquant sur les différents mots puis en envoyant
        la réponse quand vous avez fini d'écrire.

        La réponse à cette question est : « Je veux 1 point »

        Attention aux **espaces**, ils comptent dans la réponse.
        Ils sont symbolisés par des rectangles jaunes.
        """,
    good = ("Je veux 1 point", "Je veux 1 point ", " Je veux 1 point ", " Je veux 1 point"),
    bad = ("Je", "veux", "0", "1", "point", " ", "\n"),
    time = 120,
    first_day = 0.11,
    period = 999,
    keywords = True,
    )

Question("A120",
    question = """
        Les jours blancs sont les vacances et jours de repos.
        Vous pouvez répondre au QCM pour vous entrainer
        mais **cela ne changera pas votre note**.

        Est-ce que votre score changera si vous travaillez pendant les vacances :
        """,
    good = "NON",
    bad = ("OUI, mais cela peut seulement augmenter ma note",
           "OUI",
           ),
    time = 60,
    first_day = 0.12,
    period = 999,
    )

Question("A200",
    question = """
        Ce questionnaire permettra de calculer votre note de contrôle continu.
        En conséquence toute fraude sera sévèrement punie par le conseil de discipline.

        Pour détecter les fraudes, nous utiliserons plusieurs informations.
        La bonne réponse à cette question c'est l'information que nous n'utiliseront pas :
        """,
    good = ("La couleur de vos cheveux",
            "Si vous avez des lunettes de vue",
            "Votre taille en centimètres",
            "Votre poids en kilogrammes",
            ),
    bad = ("L'adresse IP utilisée",
            "Le navigateur web utilisé",
            "L'ordinateur utilisé",
            "Les dates et heures de vos réponses",
            ),
    time = 60,
    first_day = 0.9,
    period = 999,
    )
