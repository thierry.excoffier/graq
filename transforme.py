#!/usr/bin/python3

import sys

entete = True

for line in sys.stdin:
    if line.startswith('*'):
        entete = False
    if line.startswith(("+", "-")) or not entete and line.startswith(' '):
        if line.startswith(("+", "-")):
            # Grands espaces
            line = line[:2] + line[2:].lstrip().replace(" ", "  ")
        # Protège les crochets
        line = line.replace("[", "£")
        line = line.replace("]", "¤")
        line = line.replace("£", "[[\\mbox{[}]]")
        line = line.replace("¤", "[[\\mbox{]}]]")
        # Augmente l'espace autour du .
        line = line.replace(".", "[[\\kern0.1mm]].[[\\kern0.2mm]]")
        # Diminue l'espace entre / et .
        line = line.replace("/[[\\kern{0.1mm}]].", "/[[\\hspace{-0.2mm}]].")
        # Ressère les >>
        line = line.replace(">>", ">[[\\hspace{-0.2em}]]>")
        # Eloigne les {}
        line = line.replace("{}", "{[[\\hspace{0.1em}]]}")
        # Gros > et <
        line = line.replace(">", "[[$>$]]")
        line = line.replace("<", "[[$<$]]")
        # Guillemet droit
        line = line.replace('"', '[[\\texttt{"}]]')
        # Quote droit en resserant l'espace car c'est une fonte monospace
        line = line.replace("'", "[[\\hspace{-0.3mm}]][[\\texttt{'}]][[\\hspace{-0.3mm}]]")
        if line.startswith(' '):
            # Cas - avec des espaces devant
            # Espace insécable donc fixe
            line = '[[\\\\ \\hspace{1cm}{\\tt ]]' + line.rstrip().replace(' ', ' ') + '[[}]]\n'
        # line = line[:2] + '[[\\hspace{-3mm}]]' + line[2:]
    if not entete:
        # Protège les ':'
        line = line.replace(":", "[[\\mbox{:}]]")
    print(line, end="")


