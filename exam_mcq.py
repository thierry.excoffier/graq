#!/usr/bin/python3
# pylint: disable=invalid-name,missing-docstring,bad-whitespace,too-few-public-methods
"""
MCQ Exams


"""

from typing import Dict, List, Union, Iterable, Set, Any, Tuple, Generator, Optional # pylint: disable=unused-import
import os
import sys
import re
import traceback
import ast
import random
import collections
import glob
import json
class DuplicateIdentifier(ValueError):
    pass

PARAMETER = Union[int, str, List[str]]
KARGS = Dict[str, PARAMETER]
ARGS = Iterable[str]

html_style = """
<script>
var last_x ;
function highlight(x)
{
    if ( last_x )
        last_x.style.background = '#FFF' ;
    last_x = document.getElementById(x) ;
    last_x.style.background = '#FF0' ;
}
function unhighlight(x)
{
    if ( last_x )
        last_x.style.background = '#FFF' ;
}
</script>
<style>
DIV[onmouseenter] { background: #8F8 }
DIV {
    border: 1px solid black ;
    margin-left: 1em ;
    margin-right: 2px ;
    margin-top: 2px ;
    margin-bottom: 2px ;
    display: inline-block ;
    vertical-align: top ;
    transition: background 1s ;
    background: #FFF ;
    }
K { color: #FFF ; background: #000 }
C { color: #FFF ; background: #00F }
</style>
"""

re_braces = re.compile(r'\{[^{}]*\{[^{}]*}}|\{[^{}]+}')

def indent(txt:str, padding='    ') -> str:
    return padding + txt.replace('\n', '\n' + padding).rstrip()

def get_choice_sequence(good:str, keywords:List[str], start:List[int]=[]) -> Generator[List[int],None,None]: # pylint: disable=dangerous-default-value,line-too-long
    """Iterate on all the ways to create 'good' from the keyword list.
    For each one, it returns the keyword index list to get the good answer."""
    if good == '':
        yield start
    else:
        for i, keyword in enumerate(keywords):
            if good.startswith(keyword):
                yield from get_choice_sequence(good[len(keyword):],
                                               keywords,
                                               start + [i])

def escape(t):
    """Works only for Python"""
    return str(t).replace("&", '&amp;').replace(">", '&gt;').replace("<", '&lt;')

class Choice:
    id = 0
    version = -1

    def __init__(self, key:str) -> None:
        self.key = key
        self.id = Choice.id
        self.text_id = 'id' + str(Choice.id)
        Choice.id += 1

class ChoiceItem(Choice):
    item = None # type: Base

    # Once the question selection is done:
    selected    = None # type: Base
    unselecteds = None # type: List[Base]
    item_used   = None # type: List[Base]

    def select(self) -> None:
        if self.item.questionnary.version == self.version:
            return
        self.version = self.item.questionnary.version
        if self.item.all_alternatives:
            self.unselecteds = list(self.item.all_alternatives)
            random.shuffle(self.unselecteds)
            self.selected  = self.unselecteds.pop()
            self.item_used = [self.selected]
        else:
            self.selected = self.item
            self.unselecteds = []
            self.item_used = [self.selected]

    def __str__(self) -> str:
        return 'CHOICEITEM\n{}\n  SELECTED\n{}\n  UNUSED\n{}\n  USED\n{}'.format(
            self.item,
            indent(str(self.selected)),
            indent('\n'.join(sorted(i.key for i in self.unselecteds))),
            indent('\n'.join(sorted(i.key for i in self.item_used))),
            )

    def nice_dump(self, s:"List[str]") -> None:
        if self.text_id in s:
            s.append('<div onmouseenter="highlight(\'{}\')" '
                     'onmouseleave="unhighlight()"><b>{} {}</b></div>'.format(
                         self.text_id, self.__class__.__name__, self.key))
            return
        s.append('<div id="{}"><b>{} <c>{}</c> '.format(
            self.text_id, self.__class__.__name__, self.key))
        s.append(self.text_id)
        s.append('</b><br>')

        if self.selected:
            s.append('Selected:')
            self.selected.nice_dump(s)
            s.append('<br>')
        else:
            s.append('Nothing selected<br>')
        if self.unselecteds:
            s.append('Unselecteds:<br>')
            for v in self.unselecteds:
                v.nice_dump(s)
            s.append('<br>')
        else:
            s.append('Nothing to select from (unselecteds is empty)<br>')
        if self.item_used:
            s.append('Items used:<br>')
            for v in self.item_used:
                v.nice_dump(s)
            s.append('<br>')
        else:
            s.append('No item used<br>')
        s.append('</div>')

class ChoiceAttr(Choice):
    """A thing between {}"""
    # pylint: disable=too-many-instance-attributes
    negate = False
    attr_indirection = None # type: ChoiceItem
    attr = ''
    parent = None   # type: ChoiceItem
    value = ''
    reuse = None    # type: ChoiceAttr

    # Once the question selection is done:
    selected = None # type: str
    item_selected = None # type: Base
    attr_selected = None # type: str

    def __str__(self) -> str:
        return "{:20}{}{:31} {}{:10}{}".format(
            self.key,
            '!' if self.negate else ' ',
            self.parent.item.key,
            '*' if self.attr_indirection else ' ',
            self.attr,
            '{}'.format(self.parent.key) if self.parent else '')

    def select_attr(self) -> str:
        if self.attr_indirection:
            return self.attr_indirection.selected.name
        return self.attr

    def select(self) -> bool:
        if self.negate:
            for item in self.parent.unselecteds:
                attr = self.select_attr()
                for other in self.parent.item_used:
                    if str(item.kargs[attr]) == str(other.kargs[attr]):
                        break
                else:
                    self.attr_selected = attr
                    self.item_selected = item
                    item.select()
                    self.parent.item_used.append(item)
                    self.parent.unselecteds.remove(item)
                    break
            else:
                return False
        else:
            self.attr_selected = self.select_attr()
            if self.reuse:
                self.item_selected = self.reuse.item_selected
            else:
                self.item_selected = self.parent.selected
        if self.attr_selected in self.item_selected.kargs:
            # self.value = self.value = str(self.item_selected.kargs[self.attr_selected])
            self.value = self.item_selected.kargs[self.attr_selected].get_str()
            return True
        return False

    def nice_dump(self, s:"List[str]") -> None:
        if self.text_id in s:
            s.append('<div onmouseenter="highlight(\'{}\')" '
                     'onmouseleave="unhighlight()"><b>{} {}:{}</b></div>'.format(
                         self.text_id, self.__class__.__name__, self.key, self.attr))
            return
        s.append('<div id="{}"><b>{} <c>{}</c> negate={} '.format(
            self.text_id, self.__class__.__name__, self.key, self.negate))
        s.append(self.text_id)
        s.append('<k>{}</k></b><br>'.format(self.attr))

        s.append("Parent:")
        self.parent.nice_dump(s)
        s.append('<br>')
        if self.item_selected:
            s.append('Item selected:')
            self.item_selected.nice_dump(s)
            s.append('<br>')
        else:
            s.append('No Item selected<br>')
        if self.reuse:
            s.append("Reuse:")
            self.reuse.nice_dump(s)
            s.append('<br>')
        else:
            s.append('No reuse.<br>')
        if self.attr_indirection:
            s.append('Attr indirection:')
            self.attr_indirection.nice_dump(s)
            s.append('<br>')
        else:
            s.append('No attr indirection<br>')
        s.append('</div>')

class Arg:
    base = None # type: Base
    def __init__(self, base:"Base", value:Any) -> None:
        self.original = value
        self.base = base
    def get_str(self) -> str:
        return str(self.original)
    def get_float(self) -> float:
        return float(self.original)
    def get_int(self) -> int:
        return int(self.get_float())
    def get_goods(self) -> "List[str]":
        return [self.get_str()]
    def compile(self) -> None:
        pass
    def dump(self) -> str:
        return str(self.original)
    def nice_dump(self, s:"List[str]") -> None:
        s.append('<div>' + escape(self.original) + '</div>')
    def __repr__(self) -> str:
        return str(self.original)
    @classmethod
    def new(cls, base:"Base", value:Any) -> 'Arg':
        if isinstance(value, str):
            return ArgString(base, value)
        if isinstance(value, (tuple, list)):
            return ArgList(base, value)
        return Arg(base, value)

class ArgString(Arg):
    """A string with its associated choices {} {}..."""
    choices = None # type: List[ChoiceAttr]
    def compile(self) -> None:
        self.choices = []
        for key in re_braces.findall(self.original):
            self.choices.append(self.base.compile_choice(key[1:-1]))

    def get_str(self) -> str:
        s = self.original
        for choice in self.choices:
            # By choice value
            s = s.replace('{'+choice.key+'}', choice.value, 1)
        return s

    def dump(self) -> str:
        return repr(self)

    def nice_dump(self, s:"List[str]") -> None:
        s.append("<div><b>{} {}</b><br>".format(self.__class__.__name__, escape(self.original)))
        for v in self.choices:
            v.nice_dump(s)
            s.append('<br>')
        s.append("</div>")

    #def __str__(self):
    #    return self.original
    def __repr__(self) -> str:
        if self.choices:
            return repr(self.original) + ''.join('\n            ' + str(choice)
                                                 for choice in self.choices)
        return self.original

class ArgList(Arg):
    def __init__(self, base:"Base", value:Any) -> None:
        super().__init__(base, value)
        self.original = [Arg.new(self.base, value) for value in self.original]

    def compile(self) -> None:
        for v in self.original:
            v.compile()

    def nice_dump(self, s:"List[str]") -> None:
        s.append("<div><b>ARGLIST</b><br>")
        for v in self.original:
            v.nice_dump(s)
        s.append("</div>")

    def get_str(self) -> str:
        return random.choice(self.original).get_str()

    def get_goods(self) -> "List[str]":
        return [choice.get_str() for choice in self.original]

    def dump(self) -> str:
        return '\n' + '\n'.join('        ' + repr(choice) for choice in self.original)

class ArgInt(Arg):
    pass


class Base: # pylint: disable=too-many-instance-attributes
    keywords = False
    def __init__(self, questionnary:"Questionnary", key:str, args:ARGS, kargs:KARGS) -> None:
        self.questionnary = questionnary
        self.name = key
        if '_key' not in kargs:
            kargs['_key'] = key
        key = questionnary.current_filename + '.' + key
        self.filename = questionnary.current_filename
        self.key = key
        self.alternatives = list(args)
        self.kargs = {k: Arg.new(self, v) for k, v in kargs.items()}
        if key in questionnary.all:
            raise DuplicateIdentifier(key)
        questionnary.all[key] = self
        self.register()[key] = self
        self.all_alternatives = None # type: Optional[List[Base]]
        self.choices = [] # type: List[ChoiceAttr]
        # print(self.__class__.__name__, key, args, kargs)

    def get(self, key: str) -> "Optional[Base]":
        v = self.questionnary.all.get(key, None)
        if v is not None:
            return v
        v = self.questionnary.all.get(self.filename + '.' + key, None)
        if v is not None:
            return v
        return self.questionnary.all.get(self.filename.split('.')[0] + '.' + key, None)

    def get_safe(self, key: str) -> "Base":
        v = self.get(key)
        if v is None:
            raise ValueError(str(self) + '\n' + key + ' not found')
        return v

    def compile_choice_item(self, key: str) -> ChoiceItem:
        item = self.get_safe(key)
        choice = self.questionnary.choices_dict_item.get(item.key, None)
        if not choice:
            self.questionnary.choices_dict_item[item.key] = choice = ChoiceItem(key)
            choice.item = item
        return choice

    def compile_choice(self, key: str) -> ChoiceAttr:
        """Beware : the compilation is local to the question
        """
        choice = ChoiceAttr(key)
        self.choices.append(choice)
        if key[0] == '!':
            key = key[1:]
            choice.negate = True
        if ':' in key:
            item_key, attr_key = key.rsplit(':', 1)
        else:
            item_key = key
            attr_key = '_key'
        if item_key.strip(':') == '' and not choice.negate:
            choice.reuse = self.choices[-2 - len(item_key)]
            choice.parent = choice.reuse.parent
            if attr_key == '':
                attr_key = '_key'
        else:
            choice.parent = self.compile_choice_item(item_key)
        item = choice.parent.item
        item.compile()
        if item.has_key(attr_key):
            # {FILENAME.ITEM:ARG} case
            choice.attr = attr_key
            return choice
        if attr_key.startswith('{') and attr_key.endswith('}'):
            attr_key = attr_key[1:-1]
            # {FILENAME.?:{ARG}} case
            choice.attr = attr_key
            choice.attr_indirection = self.compile_choice_item(attr_key)
            return choice
        raise ValueError(str(self) + '\nuse non existant attribute ' + str(key))

    def compile(self) -> None:
        if self.all_alternatives is not None:
            return # Yet compiled
        self.all_alternatives = self.compile_all_alternatives()
        self.choices = []
        try:
            for k in sorted(self.kargs):
                self.kargs[k].compile()
        except:
            print("Fail to compile {}:{}".format(self.name, k), file=sys.stderr)
            raise

    def has_key(self, attr_key:str) -> bool:
        if self.kargs.get(attr_key, None) is not None:
            return True
        if self.all_alternatives is None:
            raise ValueError('Bug') # pragma: no cover
        for alt in self.all_alternatives:
            if alt.has_key(attr_key):
                return True
        return False

    def __str__(self) -> str:
        t = [self.key + ' ' + str(self.alternatives)]
        for k in sorted(self.kargs):
            if k.startswith('_'):
                continue
            t.append("    {} {}".format(k, self.kargs[k].dump()))
        return '\n'.join(t)

    def register(self): # pragma: no cover
        raise ValueError("Bug in " + str(self))

    def check_definition(self) -> None:
        pass

    def compile_all_alternatives(self) -> "List[Base]": # pylint: disable=no-self-use
        return []

    def nice_dump(self, s:"List[str]"):
        if self.key in s:
            s.append('<div onmouseenter="highlight(\'{}\')" '
                     'onmouseleave="unhighlight()"><b>{} {}</b></div>'.format(
                         self.key, self.__class__.__name__, self.key))
            return
        s.append('<div id="{}"><b>{} '.format(self.key, self.__class__.__name__))
        s.append(self.key)
        s.append('</b><br>')
        for k, arg in self.kargs.items():
            s.append('<k>' + escape(k) + '</k>')
            arg.nice_dump(s)
            s.append('<br>')
        s.append('Choices:<br>')
        for c in self.choices:
            c.nice_dump(s)
            s.append('<br>')
        if self.all_alternatives:
            s.append('all_alternatives:<br>')
            for v in self.all_alternatives:
                v.nice_dump(s)
                s.append('<br>')
        if self.alternatives:
            s.append('Alternatives: {}<br>'.format(
                ' '.join(escape(name for name in self.alternatives))))
        s.append('</div>')

    def dump_html(self) -> str:
        s = [html_style]
        self.nice_dump(s)
        return '\n'.join(s)

    def select_error(self) -> str:
        for choice_attr in self.choices:
            choice_attr.parent.select()
            if choice_attr.parent.selected.choices:
                choice_attr.parent.selected.select()
        for choice_attr in self.choices:
            if not choice_attr.select():
                return 'No choice'
        if 'good' in self.kargs:
            texts = []
            if not self.keywords:
                texts.append(self.kargs['good'].get_str())
            for bad in self.kargs['bad'].original:
                texts.append(bad.get_str())
            if len(texts) != len(set(texts)):
                return 'duplicate'
        return ''

    def select(self) -> None:
        self.select_error()

class Item(Base):
    def register(self) -> Dict[str, "Item"]:
        return self.questionnary.items

class Alternative(Base):
    def register(self) -> Dict[str, "Alternative"]:
        return self.questionnary.alternatives

    def compile_all_alternatives(self) -> List[Base]:
        t = []
        for a in self.alternatives:
            item = self.get_safe(a)
            item.compile()
            if item.all_alternatives:
                t.extend(item.all_alternatives)
            else:
                t.append(item)
        return t

class Subtract(Alternative):
    def compile_all_alternatives(self) -> List[Base]:
        first = self.get_safe(self.alternatives[0])
        first.compile()
        if first.all_alternatives is None:
            raise ValueError('Bug') # pragma: no cover
        t = list(first.all_alternatives)
        for a in self.alternatives[1:]:
            item = self.get_safe(a)
            item.compile()
            if item.all_alternatives:
                for i in item.all_alternatives:
                    if i in t:
                        t.remove(i)
            else:
                if item in t:
                    t.remove(item)
        return t

class Question(Base):
    hate_question = 0
    hate_bad = 0
    hate_good = 0
    all_answers = [] # type: List[Tuple[bool,float]]

    def register(self) -> Dict[str, "Question"]:
        return self.questionnary.questions

    def check_definition(self) -> None:
        assert 'question' in self.kargs
        assert 'good' in self.kargs
        assert 'bad' in self.kargs
        assert isinstance(self.kargs['bad'], ArgList)
        assert self.kargs['good'].original or self.kargs['good'].original == 0
        self.keywords = 'keywords' in self.kargs
        if 'first_day' not in self.kargs:
            self.kargs['first_day'] = Arg.new(self, 0)
        self.all_answers = []

    def get_string(self) -> str:
        """For AMC Auto Complete Choice"""
        def length(txt):
            return len(re.sub('[A-Z\\\\]', '??', re.sub('[<>~]', '???', txt))) + 5
        bads = [bad.get_str()
                for bad in self.kargs['bad'].original]
        good = self.kargs['good'].get_str()
        width = max(max(length(bad) for bad in bads), length(good))
        nr_columns = max(min(min(10, int(70 / width + 0.5)), len(bads)+1), 1)
        if 'columns' in self.kargs:
            nr_columns = self.kargs['columns'].get_int()
        t = ['']
        s = '*[id={}'.format(self.name)
        if 'group' in self.kargs:
            s += ',group=' + self.get_group()
        if '--no-column' not in sys.argv:
            s += ',columns={}'.format(nr_columns)
        s += '] '
        s += re.sub('\n', ' ', self.kargs['question'].get_str())
        t.append(s)
        t.append('+ ' + good)
        for bad in bads:
            t.append('- ' + bad)
        return '\n'.join(t)

    def get_float(self, attr_name:str, defaults:Dict[str,Any]) -> float:
        if attr_name in self.kargs:
            return self.kargs[attr_name].get_float()
        return float(defaults.get(attr_name, 0))
    def get_int(self, attr_name:str, defaults:Dict[str,Any]) -> int:
        return int(self.get_float(attr_name, defaults))
    def get_group(self) -> str:
        if 'group' in self.kargs:
            return self.kargs['group'].get_str()
        return ''

    def get_state(self, defaults:Dict[str,Any]) -> Tuple[Dict[str,Any], List[List[int]], str]:
        choices = [bad.get_str()
                   for bad in self.kargs['bad'].original
                  ]
        good = self.kargs['good'].get_str()
        if not self.keywords:
            choices.append(good)
        if self.questionnary.regression_tests:
            choices.sort()
        random.shuffle(choices)

        d = {
            'time': self.get_float('time', defaults),
            'time_scale': self.get_float('time_scale', defaults),
            'question': self.kargs['question'].get_str(),
            'choices': choices,
            }
        if self.get_int('keep_space', defaults):
            d['keep_space'] = True
        if self.keywords:
            d['keywords'] = True
            accepted = []
            for good in self.kargs['good'].get_goods():
                answers = tuple(get_choice_sequence(good, choices))
                if not answers: # pragma: no cover
                    raise ValueError(
                        "{} : Impossible to enter the good answer: «{}» with {}".format(
                            self.key, good,
                            ' '.join('«'+i.get_str()+'»' for i in self.kargs['bad'].original)))
                if len(answers) != 1: # pragma: no cover
                    raise ValueError("Multiple good choices for " + good)
                accepted.append(answers[0])
            d['collapse_spaces'] = self.get_int('collapse_spaces', defaults)
            return d, accepted, good
        return d, [[choices.index(good)]], good

    def update_stats(self, good:bool, dt:float):
        self.all_answers.append((good, dt))

    def get_stats(self, defaults) -> List[str]:
        n = [[0, 0], [0, 0]]
        t = self.get_int('time', defaults)
        for good, dt in self.all_answers:
            n[int(good)][int(dt > t)] += 1
        nr_bads = sum(n[0])
        nr_goods = sum(n[1])
        nr = nr_bads + nr_goods
        if nr_bads == 0 and nr_goods == 0:
            s = ['?'] * 6
        else:
            s = [ '{:3d}'.format(int(100 * i / nr))
                  for i in (nr_bads, *n[0], nr_goods, *n[1])
                ]
        s.append(str(self.get_int('time', defaults)))
        s.append(str(nr))
        s.append(str(self.hate_question))
        s.append(str(self.hate_good))
        s.append(str(self.hate_bad))
        s.append(str(self.hate_question + self.hate_good + self.hate_bad))
        return s

    def select(self) -> None:
        duplicate = True
        for _ in range(1000):
            self.questionnary.version += 1 # clear all selections
            error = self.select_error()
            if not error:
                return
            if error != 'duplicate':
                duplicate = False
        if duplicate:
            raise ValueError("Always identical choices for {}".format(self))
        raise ValueError("No choice possible for {}".format(self))

State = Tuple[Dict[str, Base],Dict[str, Question],Dict[str, Alternative], Dict[str, Item]]

class Questionnary: # pylint: disable=too-many-instance-attributes
    current_filename = ''
    state = ({}, {}, {}, {}) # type: State
    regression_tests = False
    slots_per_day = 1
    session_start = 0
    version = 0

    def __init__(self) -> None:
        self.all = {}           # type: Dict[str, Base]
        self.questions = {}     # type: Dict[str, Question]
        self.items = {}         # type: Dict[str, Item]
        self.alternatives = {}  # type: Dict[str, Alternative]
        self.functions = {
            'Item'       : self.add_item,
            'Alternative': self.add_alternative,
            'Subtract'   : self.add_subtract,
            'Question'   : self.add_question,
            'letters'    : self.letters
            }
        self.choices_dict_item = {} # type: Dict[str, ChoiceItem]

    def letters(self, txt, name=None):
        t = []
        for letter in txt:
            key = '_' + letter
            t.append(key)
            try:
                self.add_item(key,
                              _key=letter,
                              name='«' + letter + '»',
                              lower=letter.lower(),
                              upper=letter.upper(),
                             )
            except DuplicateIdentifier:
                pass # Allow to reuse the same letter in multiple alternatives
        self.add_alternative(name or txt, *t)

    def load_files(self, *filenames: str) -> str:
        # Not a reentrant function
        self.push_state()
        error = ''
        try:
            for filename in filenames:
                if filename.endswith('.py'):
                    filename = filename[:-3]
                with open(filename.replace('.', os.path.sep) + '.py', 'r') as f:
                    source = f.read()
                self.current_filename = filename.replace(os.path.sep, '.')
                eval(compile(source, filename, 'exec'), self.functions) # pylint: disable=eval-used
                self.current_filename = ''
            self.compile()
            try:
                for item in self.all.values():
                    item.check_definition()
            except AssertionError:
                print(item, file=sys.stderr)
                raise
        except: # pylint: disable=bare-except
            error = traceback.format_exc()
            self.pop_state()
        return error

    def push_state(self) -> None:
        self.state = (dict(self.all), dict(self.questions),
                      dict(self.alternatives), dict(self.items))

    def pop_state(self) -> None:
        self.all, self.questions, self.alternatives, self.items = self.state
        self.compile()

    def add_item(self, key:str, **kargs:PARAMETER) -> None:
        Item(self, key, [], kargs)

    def add_alternative(self, key:str, *args:str, **kargs:PARAMETER) -> None:
        Alternative(self, key, list(args), kargs)

    def add_subtract(self, key:str, *args:str, **kargs:PARAMETER) -> None:
        Subtract(self, key, list(args), kargs)

    def add_question(self, key:str, **kargs:PARAMETER) -> None:
        Question(self, key, [], kargs)

    def stats(self) -> str:
        return '\n'.join((
            "{} : {} questions, {} alternatives, {} items".format(
                len(self.all), len(self.questions),
                len(self.alternatives), len(self.items)),
            "{} ChoiceItem+ChoiceAttr".format(Choice.id)))

    def stats_day(self) -> str:
        t = [0] * 365
        for q in self.questions.values():
            t[int(q.kargs['first_day'].get_float())] += 1
        while t[-1] == 0:
            t.pop()
        s = []
        total = 0
        for i, v in enumerate(t):
            total += v
            s.append("{}\t{}\t{}\t{:.1f}".format(
                i, v, total, (i+1) * self.slots_per_day / total))
        return '\n'.join(s)

    def compile(self) -> None:
        Choice.id = 0
        for item in self.all.values():
            item.all_alternatives = None
        for key in sorted(self.all):
            self.all[key].compile()

    def __str__(self) -> str:
        return '\n'.join(str(self.all[k]) for k in sorted(self.all))

    def groups(self) -> Dict[str, List[Question]]:
        grps = collections.defaultdict(list) # type: Dict[str, List[Question]]
        for question in self.questions.values():
            grps[question.get_group()].append(question)
        return grps

def main(): # pragma: no cover
    q = Questionnary()
    files = ['TESTS/session.py']
    reject = []
    current = files
    for arg in sys.argv[1:]:
        if arg.isdigit():
            random.seed(int(arg))
        elif arg == '--':
            current = reject
        elif arg.startswith('--'):
            pass
        else:
            current.append(arg)
    error = q.load_files(*[filename[:-3] if filename.endswith('.py') else filename
                           for filename in files]
                        )
    if error:
        print(error)
        sys.exit(1)
    replacements = ast.literal_eval(
        q.items['TESTS.session.default'].kargs['replacements'].get_str())

    groups = q.groups()
    for v in groups.values():
        random.shuffle(v)

    for qq in q.questions.values():
        if qq.name in reject:
            continue
        group = qq.get_group()
        if group:
            # Take only the last of the shuffled group
            others = groups[group]
            if others[-1] is not qq:
                continue
        qq.select()
        s = qq.get_string()
        for regexp, value in replacements:
            s = re.sub(regexp, value, s)
        print(s)
    sys.exit(0)



def competences():
    import login
    import server

    del sys.argv[1]

    server.HTTPProtocol.finish_init()

    class FakeSession:
        OPTIONS = server.HTTPProtocol.OPTIONS
        DEFAULTS = server.HTTPProtocol.DEFAULTS
        QUESTIONNARY = server.HTTPProtocol.QUESTIONNARY

    competence_definitions = json.loads(FakeSession.DEFAULTS['competences'])
    all_competences = {infos[0]: i for i, infos in enumerate(competence_definitions)}
    for key, val in FakeSession.QUESTIONNARY.questions.items():
        for competence in str(val.kargs.get('competence', '')).split(' '):
            if competence not in all_competences and competence:
                print(competence, 'not found in', all_competences)
                bug

    for filename in glob.glob('LOGS/*.log'):
        # if 'p2303970' not in filename:
        #     continue
        infos = login.Login(filename.split('/')[1][:-4], 0, FakeSession)
        print(infos.login, end='')
        competences = collections.defaultdict(list)
        for key, val in infos.QUESTIONNARY.questions.items():
            if infos.stats[key].answers:
                value = 0
                for good in infos.stats[key].answers:
                    value *= 0.8
                    if good:
                        value = (value + 1) / 2
                for competence in str(val.kargs.get('competence', '')).split(' '):
                    competences[competence].append(value)
        for competence in sorted(all_competences):
            val = competences[competence]
            if val:
                if len(val) > 4:
                    val = int(1 + 5 * sum(val)/len(val))
                else:
                    val = int(1 + 4 * sum(val)/len(val))
                print(f' +{all_competences[competence]}o{val}', end='')
            else:
                print(f' +{all_competences[competence]}o0', end='')
        print()
    for i, infos in enumerate(competence_definitions):
        data = [infos[1], '', '', '', '', '', '']
        print(f"competenceTable.catalog.items['+{i}'] = new CatalogItem('+{i}',{json.dumps(data)});")
        print(f"competenceTable.catalog.items['+{i}'].update();")
    children = ['+'+str(i) for i in range(len(competence_definitions))]
    print(f"competenceTable.catalog.items['f44']._children = {json.dumps(children)};")






def demo(): # pragma: no cover
    q = Questionnary()
    print(q.load_files("TESTS.animals", "TESTS.mythical", 'TESTS2.animals'))
    print(q)
    for qq in q.questions.values():
        qq.select()
        print(qq.get_string())
    xxx = q.questions['TESTS.animals.AnimalQuestion']
    versions = set() # type: Set[str]
    for i in range(2):
        xxx.select()
        yyy = xxx.get_string()
        yyy2 = '\n'.join(sorted(yyy.split('\n')))
        if yyy2 not in versions:
            print('=====', i, len(versions))
            print(yyy)
            versions.add(yyy2)

if __name__ == "__main__": # pragma: no cover
    if len(sys.argv) > 1:
        if sys.argv[1] == 'competences':
            competences()
        else:
            main()
    else:
        demo()
