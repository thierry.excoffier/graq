

Item("cat"            , legs = "4 paws"   , cover = "fur"    , eat = "mices and fishes")
Item("horse"          , legs = "4 hooves" , cover = "fur"    , eat = "grass")
Item("human"          , legs = "2 foots"  , cover = "hair"   , eat = "chocolate")
Item("snail"          , legs = "1 foot"   , cover = "shell"  , eat = "salad")
Item("spider"         , legs = "8 legs"   , cover = "hair"   , eat = "flies")
Item("bird"           , legs = "2 wings"  , cover = "feather", eat = "seeds and insects")
Item("shark"          , legs = "some fins", cover = "nothing", eat = "fishes")
Item("flying squirrel", legs = "4 paws"   , cover = "fur"    , eat = "nuts")
Alternative("ANIMAL", "cat", "horse", "human", "snail", "bird", "spider", "shark",
            "TESTS2.animals.crocodile", 'mythical.dragon')

Question("AnimalLegs",
    # The question to display
    question = "The {ANIMAL} has:",
    # The good answer: {ANIMAL} is the same one than in the question
    good = "{ANIMAL:legs}",
    # Some bad answers: each {!ANIMAL} is different and
    # none of the bad answer is equal to the good answer or to each other.
    bad = ("{!ANIMAL:legs}", "{!ANIMAL:legs}", "{!ANIMAL:legs}", "5 pods"),
    # Maximum thinking time
    time = 5,
    # First day to display the question.
    first_day = 1,
    # Initial asking period in days.
    period = 2,
    # For AMC: to override the computed number of columns
    columns = 1,
    # For AMC TOMUSS: the question group
    group="easy"
    )

Question("WhichAnimalLegs",
    question = "Which is the animal with {ANIMAL:legs}:",
    good = "The {ANIMAL}",
    bad = ("The {!ANIMAL}", "The {!ANIMAL}", "The {!ANIMAL}"),
    )

Question("AnimalEat",
    question = "The prefered food of {ANIMAL} is:",
    good = "{ANIMAL:eat}",
    bad = ["{!ANIMAL:eat}",
    "{!ANIMAL:eat} and {!ANIMAL:eat}", # 2 differents animals 
    "{!ANIMAL:eat} and {:cover}", # the sames animals
    "wood", "earth"],
    period = 2,
    )

Question("AnimalCover",
    question = "The {ANIMAL} is covered with:",
    good = "{ANIMAL:cover}",
    bad = ["{!ANIMAL:cover}", "{!ANIMAL:cover}", "scale", "iron"],
    )

# Same animal in the bad answers than in the good.

Question("AnimalError",
    question = "Where is the error:",
    good = "{ANIMAL} has {!ANIMAL:legs}",
    bad = ["{ANIMAL} is covered with {ANIMAL:cover}",
            "{ANIMAL} prefered food is {ANIMAL:eat}",
          ]
    )

Question("AnimalGood",
    question = "Where is the correct choice:",
    good = "{ANIMAL} has {ANIMAL:legs}",
    bad = ["{ANIMAL} is covered with {!ANIMAL:cover}",
            "{ANIMAL} prefered food is {!ANIMAL:eat}",
          ]
    )

# Alternative combination : the combinaison of the first 3 questions

Item("legs" , text = "has")
Item("eat"  , text = "prefered food is")
Item("cover", text = "is covered with")
Alternative("WHAT", "legs", "eat", "cover")

Question("AnimalQuestion",
    question = "The {ANIMAL} {WHAT:text}:",
    good = "{ANIMAL:{WHAT}}",
    bad = ("{!ANIMAL:{WHAT}}", "{!ANIMAL:{WHAT}}", "{!ANIMAL:{WHAT}}"),
    )

# 

Question("Use Constant",
    question = "Why {horse} eats {horse:eat} ?",
    good = "Because",
    bad = ('No idea',),
    )

Question("Use Constant 2",
    question = "The {horse} eats {horse:eat} is covered with {horse:cover} and has how many legs",
    good = 4,
    bad = (0, 1, 2, 3, 5),
    )

# Alternative of Alternatives

Alternative("ANY", "ANIMAL", "WHAT")

Question("any",
    question = "{ANY:eat}", # Only animals
    good = "{ANY}",
    bad = ["{!ANY}","{!ANY}","{!ANY}","{!ANY}","{!ANY}"]
    )

# Consume all the negatives

Question("negatives",
    question = "Question",
    good = "Answer",
    # XXX Not 3 "{!WHAT}" because one positive is mandatory
    # The buggy positive came from:
    # if choice_item.selected.choices:
    #     choice_item.selected.select()
    bad = ("{!WHAT}",  "{!WHAT}"),
    )

# Alernatives for good

Alternative("FLYER", "bird", "mythical.dragon", "flying squirrel")
Subtract("NOT_FLYER",  "ANIMAL", "FLYER", "spider") # Some spider may fly

Question("goods",
    question = "Which is the flying animal",
    good = ("{FLYER}", "bee", "bat"),
    # XXX Not * 6 because one positive is mandatory
    bad = ("{!NOT_FLYER}",) * 5,
    )

# To not read, only here to raise a possible bug

Item("zzz1", name="1")
Item("zzz2", name="2")
Item("zzz3", name="3")
Item("zzz4", name="4")
Alternative("ZZZ", "zzz1", "zzz2", "zzz3", "zzz4")
Question("AAA",
    question = "zzz {ZZZ:name}",
    good = "g1 {ZZZ}",
    bad = ("b1 {ZZZ}", "b2 {ZZZ}")
    )

Question("BBB",
    question = "zzz {ZZZ:name}",
    good = "g1 {ZZZ}",
    bad = ("b1 {!ZZZ}", "b2 {:}")
    )

Question("Keywords",
    question = "Answer is {FLYER}+{NOT_FLYER}",
    good = "{FLYER}+{NOT_FLYER}",
    bad = ("{FLYER}", "+", "{NOT_FLYER}", "{!FLYER}", "{!NOT_FLYER}"),
    keywords = True,
    keep_space = True, # Do not replace spaces by «Espace»
    )

Question("allgoods",
    question = "Answer «{ANIMAL}{WHAT}»",
    good = ['{ANIMAL}{WHAT}', '{WHAT}{ANIMAL}'],
    bad =  ('{ANIMAL}', '{WHAT}'),
    keywords = True,
    )

