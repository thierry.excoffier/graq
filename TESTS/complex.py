for i in range(10):
    Item('mychoice' + str(i))
Alternative('CHOICE', *['mychoice' + str(i) for i in range(10)])

Item('alternative', attr="A choice {CHOICE}")

Question("Complex1",
    question = "A choice {CHOICE} = {alternative:attr}",
    good = "True",
    bad = ("==={CHOICE}===",),
    )


Alternative('ALTERNATIVE', 'alternative')

Question("Complex2",
    question = "A choice {CHOICE} = {ALTERNATIVE:attr}",
    good = "True",
    bad = ("==={CHOICE}===",),
    )

Item('alternative2', attr="A choice {CHOICE}")
Alternative('ALTERNATIVE2', 'alternative2')
Question("Complex3",
    question = "{ALTERNATIVE:attr} = {ALTERNATIVE2:attr}",
    good = "True",
    bad = ("False",),
    )

Question("NoChoice",
    question = "NoChoice",
    good = "True",
    bad = ("True",),
    )

Question("NoChoice2",
    question = "NoChoice2",
    good = "NoChoice2",
    bad = ("{!alternative}",)*10,
    )

Item("XA", v='A' )
Item("XB", v='B')
Alternative('X', 'XA', 'XB')

Item("YC", v='C{X:v}')
Item("YD", v='D{X:v}')
Alternative("Y", "YC", 'YD')

Question('Z',
    question="«{Y:v}»[-1] must be equal to «{!Y:v}»[-1]",
    good="T",
    bad=("F",)
    )

