
Item("aimer" , racine="aim" , e='' )
Item("manger", racine="mang", e='e')
Alternative('VERBE-ER', 'aimer', 'manger')

Item("je"   , quoi="première personne du singulier" , p='e'              , f='erai')
Item("tu"   , quoi="seconde personne du singulier"  , p='es'             , f='eras')
Item("il"   , quoi="troisième personne du singulier", p='e'              , f='era')
Item("nous" , quoi="première personne du pluriel"   , p='{VERBE-ER:e}ons', f='erons')
Item("vous" , quoi="seconde personne du pluriel"    , p='ez'             , f='erez')
Item("ils"  , quoi="troisième personne du pluriel"  , p='ent'            , f='eront')
Alternative("PRONOM", "je", "tu", "il", "nous", "vous", "ils")

Item("p", quoi="présent de l'indicatif")
Item("f", quoi="futur simple de l'indicatif")
Alternative("TEMPS", "p", "f")

Question('ER',
    question="Conjuguez «{VERBE-ER}» à la {PRONOM:quoi} au {TEMPS:quoi}",
    good="{PRONOM} {VERBE-ER:racine}{PRONOM:{TEMPS}}",
    bad=(
        "{PRONOM} {VERBE-ER:racine}{PRONOM:f}",
        "{PRONOM} {VERBE-ER:racine}{!PRONOM:{TEMPS}}",
        "{!PRONOM} {VERBE-ER:racine}{::{TEMPS}}",
        )
    )

Question('ER2',
    question="Conjuguez «{VERBE-ER}» à la {PRONOM:quoi} au {TEMPS:quoi}",
    good="{PRONOM} {VERBE-ER:racine}{PRONOM:{TEMPS}}",
    bad=('{VERBE-ER:racine}', '{PRONOM} ',
         '{!PRONOM} ', '{!PRONOM} ',  '{!PRONOM} ',
         'e'  , 'erai',
         'es' , 'eras',
                'era',
         'ons', 'erons',   
         'ez' , 'erez',
         'ent', 'eront',
        ),
    keywords=True,
    )
