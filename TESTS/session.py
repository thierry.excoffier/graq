# Default session parameters

Item('default',

     # Session attributes
     start_date = '2019-12-04',
     holidays = '', # A list of space separated dates
     name = "MCQ Demo",
     days = 120, #  Counting holidays
     nr_days_to_answer = 3, # Minimum value allowed is 1 day to answer
     time_scale = 1, # Multiply all question time by this factor
     slot_x = 4,
     slot_y = 3,
     see_secrets  = False,
     see_icons    = False,
     see_grades   = False,
     see_activity = False,
     see_stats    = False,
     see_log      = False,
     allow_exit   = False,
     grade_max = 20,
     fontFamily = 'sans',
     weekend = '[]',
     secret = '', # Modify to change the secret tickets

     # Default question attributes
     first_day = 0, # Ask the question the session's first day
     period = 1, # Ask again the next day 
     time = 10, # Ten seconds to answer
     if_bad = 2, # Divide the period by 2 on bad answer
     # period *= min( (1 + time / answer_time) * if_good,    max_increase )
     if_good = 1,
     max_increase = 4,
     # Replace strings, JS regexp
     replacements = r"[['\\bde +les\\b', 'des'], ['\\bde +le\\b', 'du']]",
     )

# Special parameters for one login: the override the global ones

Item('debug',
     nr_days_to_answer = 2,
     see_secrets  = True,
     see_icons    = True,
     see_grades   = True,
     see_activity = True,
     see_stats    = True,
     allow_exit   = True,
     see_log      = True,
     debug=True,
    )

Item('thierry.excoffier',
     nr_days_to_answer = 2,
     see_secrets  = True,
     see_icons    = True,
     see_grades   = True,
     see_activity = True,
     see_stats    = True,
     see_log      = True,
     debug=True,
    )

Item('regtests',
     nr_days_to_answer = 2,
     slot_x = 2,
     slot_y = 1,
    )

Item('future',
     start_date = '2037-12-31',
    )

Item('stats',
     see_stats = True,
     )

Item('weekend',
     weekend='[5,6]',
    )
