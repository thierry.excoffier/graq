.. role:: rawhtml(raw)
   :format: html

:rawhtml:`<style>
BODY { max-width: 40em ; text-align: justify }
H1, H2 { text-align: center }
LI { margin-top: 0.2em }
CODE { font-size: 120% }
</style>`




==========================================================================
GRAQ GRAding Questionnary
==========================================================================

Les objectifs sont :

  * De faire travailler les étudiants régulièrement.

  * Leur faire faire du bachotage pour apprendre les
    connaissances/réflexes de base.

  * Les noter individuellement.

  * Ne pas avoir besoin de les surveiller.

  * Pouvoir suivre leur travail.

L'idée est simple :

  * Des exercices à faire en temps très limité une fois
    que la question est affichée.

  * Les exercices sont des QCM à réponse unique ou
    des textes libres à composer à partir de mot-clef.

  * Au minimum une douzaine d'exercices par jour.

  * 0 pour les exercices qui n'ont pas été faits.

  * Au maximum 3 jours de retard pour faire les exercices.

  * Des outils pour détecter les tricheurs.

Voici comment faire cela simplement (pour les informaticiens).


Installation et exécution
--------------------------

Rien de plus facile :

  * Récupérer les sources :
    ::

      git clone https://forge.univ-lyon1.fr/thierry.excoffier/graq.git

  * Préparer votre machine :
    ::

      apt install python3-aiohttp python3-coverage mypy pylint3

  * Tester si tout fonctionne bien sur votre machine :
    ::

      make

  * Lancer le serveur :
    ::

      make start

  * Visiter l'URL affichée :code:`http://127.0.0.1:8080` ou :code:`http://192.168.0.1:8080`
    c'est celle de l'utilisateur  :code:`debug` qui a tous les droits.
    Attention : 192.168.0.1 est utilisable par n'importe qui sur votre réseau local.

  * Tester avec d'autres utilisateurs : :code:`http://127.0.0.1:8080/?ticket=toto`
    ou :code:`http://192.168.0.1:8080/?ticket=titi`

Options de lancement indiquées dans le :code:`Makefile` :

  * Activation de l'authentification CAS : :code:`--cas`

    Par exemple :code:`https://cas.univ-lyon1.fr/cas`

  * Ouvrir le port vers l'extérieur (donc pas 127.0.0.1) : :code:`--ip`

  * Choix des paramètres de session : :code:`--session`

    :code:`TESTS/session.py` est un fichier exemple qui contient
    les paramètres par défaut de la session et éventuellement ceux d'utilisateurs particuliers.

  * Choix des questions :

    * :code:`START/start.py` : des questions pour apprendre à utiliser l'application.

    * Vous pouvez créer votre propre répertoire pour mettre vos fichiers de question
      et les indiquer : :code:`MES_QUESTIONS/*.py`


Créer des connaissances
--------------------------

Une connaissance est un nom associé à des attributs. Par exemple :

.. code-block:: python

  Item("cat"  ,legs="4 paws"        ,cover="fur"    ,eat="mices" ,french="chat")
  Item("dog"  ,legs="4 paws"        ,cover="fur"    ,eat="meat"  ,french="chien")
  Item("bird" ,legs="2 legs 2 wings",cover="feather",eat="seeds" ,french="oiseau")
  Item("shark",legs="some fins"     ,cover="nothing",eat="fishes",french="requin")

  Item("tree" ,francais="arbre", size='big')
  Item("grass",francais="herbe", size='small')

Les noms des connaissances ne doivent pas contenir de caractères spéciaux.

Pour générer des questions de manière combinatoire, on voudra faire des alternatives entre
des connaissances équivalentes.

.. code-block:: python

  Alternative("ANIMAL", "cat", "dog", "bird", "shark")
  Alternative("VEGETAL", "tree", "grass")
  Alternative("LIVING", "ANIMAL", "VEGETAL")

  Alternative("FLYING_ANIMAL", "bird")
  Subtract("NOT_FLY_ANIMAL", "ANIMAL", "FLYING_ANIMAL")

Nommage des connaissances
--------------------------

Si la connaissance est dans un autre fichier du même répertoire
il suffit d'écrire :code:`NomFichier.NomConnaissance`

Si la connaissance est dans un fichier dans un autre répertoire,
on peut écrire par exemple :code:`TESTS.animals.cat` qui est une
connaissance qui existe dans les fichiers fournis.



Créer des questions
--------------------------

L'ensemble des chaines de caractère qui permettent de définir
une question est analysée de manière globale afin de garantir
que toutes les mauvaises réponses sont différentes et
ne sont pas égales à la bonne réponse.

Des remplacements sont faits dans les chaînes de caractères.

  * :code:`{xxx}`      : :code:`xxx` tel quel (sauf si :code:`_key` est défini)
  * :code:`{XXX}`      : La valeur d'une des alternatives
  * :code:`{xxx:attr}` : La valeur de l'attribut :code:`attr` de :code:`xxx`
  * :code:`{XXX:attr}` : La valeur de l'attribut d'une des alternatives

Une fois qu'une alternative est choisie pour une version de la question,
c'est toujours la même qui est réutilisée.

  * :code:`{!XXX}`      : La valeur d'une autre alternative que celle choisie
  * :code:`{!XXX:attr}` : L'attribut d'une des autres alternatives

Une fois qu'une autre alternative a été choisie, elle ne le sera plus.
Si l'on veut réutiliser la même autre alternative, on peut faire :

  * :code:`In french it's {!ANIMAL}, it eats {:eat} and is covered with {:cover}`
  * The :code:`{!ANIMAL} is in the house and the {!VEGETAL} in the garden.
    The {::} eats {:eat} and the {:::} is {:size}`
    Le nombre de deux points indique la distance de l'élément que
    l'on réutilise.

L'attribut peut lui même être choisi au hasard dans une alternative :

.. code-block:: python

  Item('eat', text="eats")
  Item('cover', text="is covered with")
  Alternative("ANIMAL_ATTR", "eat", "cover")

  "The {ANIMAL} {ANIMAL_ATTR:text} {ANIMAL:{ANIMAL_ATTR}}"


Voici la question minimale sous forme QCM :

.. code-block:: python

  Question("Legs", # A short identifier, that must not be changed
    question = "The {ANIMAL} has:",
    good = "{ANIMAL:legs}",
    bad = ("{!ANIMAL:legs}", "{!ANIMAL:legs}", "5 pods"),
    )

On peut varier les bonnes réponses, mais une seule sera proposée et acceptée :

.. code-block:: python

  Question("6", # A short identifier
    question = "Qu'est ce qui donne 6 ?",
    good = ("2*3", "3+3", "5+1", "7-1"),
    bad = ("2+3", "3-3", "10-3"),
    )

On peut proposer une réponse libre avec un vocabulaire limité :

.. code-block:: python

  # On suppose définies les alternatives suivantes :
  # CMD2     : les commandes manipulant 2 fichiers.
  # A        : un premier ensemble de noms de fichiers
  # B        : un deuxième ensemble de noms de fichiers
  Question("Copy", # A short identifier
    question = "Quelle est la commande pour {CMD2:texte} {A} dans {B} ?",
    good = "{CMD2} {A} {B} ",
    bad = ("{CMD2} ", "{A} ", "{B} ",
           "{!CMD2} ", "{!CMD2} ", "{!CMD2} ", # Mauvaises CMD2s
           "{A}/{B} ", "/{A} ", "/{B} "),
    keywords = True,
    )

Si :code:`keywords=True` alors :code:`good` peut définir une liste
de bonnes réponses qui seront toutes acceptées.

Les différents textes définissant une question peuvent contenir du *markdown* :

  * Un mot :code:`**gras**` et un :code:`__souligné__`
  * Une ligne vide représente un nouveau paragraphe.
  * Les :code:`https:...` sont transformés en lien.
  * Les «\*» indentées représentent un item d'une liste simple.
  * Si une question ou des choix doivent contenir des accolades,
    alors utilisez ces caractères à la place : ﹛ ﹜ au moment
    de l'affichage ils seront remplacés par de vraies accolades.

Les autres attributs possibles pour les questions sont :

  * :code:`time` : le temps maximum de réflexion en seconde pour obtenir 1 point,
    après ce temps, le point perd progressivement sa valeur.
    Si l'on met deux fois plus de temps, on ne gagne que 0.5 points.

  * :code:`first_day` : le numéro du premier jour ou sera posé la question.
    On peut indiquer des valeurs en nombres flottants.
    Cela permet d'ordonner les questions lors de leur premier affichage.

  * :code:`period` : nombre de jours dans lequel sera reposé la question.
    Ce nombre est divisé par deux à chaque mauvaise réponse et il augmente
    proportionnellement à la rapidité de la réponse de l'étudiant.

  * :code:`columns` : utilisé seulement quand on génère un fichier AMC.

  * :code:`keep_space` : quand :code:`keywords` est utilisé, le caractère
    espace est remplacé par un bloc coloré dans les boutons.
    Sauf si :code:`keep_space=True`


:code:`letters(Iterable[str], alternative_name:str)` va créer une alternative
et des :code:`Item` qui auront les attributs :

  * :code:`lower` la chaine passée en minuscules.
  * :code:`upper` majuscules.
  * :code:`name` la chaine entre « ».

Générer des compétences automatiquement
.......................................

Si vous voulez générer automatiquement des compétences à importer
dans TOMUSS ajoutez comme attribut de question une liste de code de compétences
séparés par des espaces.
Les compétences sont simplement moyennées entre question,
mais en interne à chaque question les premières réponses ont
moins de poids que les dernières (voir :code:`exam_mcq.py`)


.. code-block:: python

  competence = "shell pattern",

Les codes de compétences et leurs intitulés détaillés
sont indiqués lors de la création de la session.

L'extraction des compétences est faite via la commande
qui doit indiquer la description de la session et des questions :

.. code-block:: shell

  ./exam_mcq.py competences --session X/def_session.py    X/animals.py X/vegetals.py



Une exemple avec les conjugaisons régulières
............................................

Cet exemple est dans :code:`TESTS/conjugue.py` :

.. code-block:: python

  Item("aimer" , racine="aim" , e='' )
  Item("manger", racine="mang", e='e')
  Alternative('VERBE-ER', 'aimer', 'manger')
  
  Item("je"   , quoi="première personne du singulier" , p='e'              , f='erai')
  Item("tu"   , quoi="seconde personne du singulier"  , p='es'             , f='eras')
  Item("il"   , quoi="troisième personne du singulier", p='e'              , f='era')
  Item("nous" , quoi="première personne du pluriel"   , p='{VERBE-ER:e}ons', f='erons')
  Item("vous" , quoi="seconde personne du pluriel"    , p='ez'             , f='erez')
  Item("ils"  , quoi="troisième personne du pluriel"  , p='ent'            , f='eront')
  Alternative("PRONOM", "je", "tu", "il", "nous", "vous", "ils")
  
  Item("p", quoi="présent de l'indicatif")
  Item("f", quoi="futur simple de l'indicatif")
  Alternative("TEMPS", "p", "f")
  
  Question('ER',
      question="Conjuguez «{VERBE-ER}» à la {PRONOM:quoi} au {TEMPS:quoi}",
      good="{PRONOM} {VERBE-ER:racine}{PRONOM:{TEMPS}}",
      bad=(
          "{PRONOM} {VERBE-ER:racine}{PRONOM:f}",
          "{PRONOM} {VERBE-ER:racine}{!PRONOM:{TEMPS}}",
          "{!PRONOM} {VERBE-ER:racine}{::{TEMPS}}",
          )
      )

Il faut pour avoir des phrases correctes mettre dans les remplacements
dans le paramètres de session :

.. code-block:: python

     replacements = r"[['\\bje a', 'j\'a']]",


Si vous lancez :code:`./exam_mcq.py TESTS/conjugue.py`
cela va générer cette question AMC aléatoire :

.. code-block:: 

  *[columns=4] Conjuguez «manger» à la seconde personne du pluriel au présent de l'indicatif
  + vous mangez
  - vous mangerez
  - vous manges
  - nous mangeons

Si vous voulez que l'étudiant invente lui-même ses mauvaises réponses :

.. code-block:: python

  Question('ER2',
      question="Conjuguez «{VERBE-ER}» à la {PRONOM:quoi} au {TEMPS:quoi}",
      good="{PRONOM} {VERBE-ER:racine}{PRONOM:{TEMPS}}",
      bad=('{VERBE-ER:racine}', '{PRONOM} ',
           '{!PRONOM} ', '{!PRONOM} ',  '{!PRONOM} ',
           'e'  , 'erai',
           'es' , 'eras',
                  'era',
           'ons', 'erons',
           'ez' , 'erez',
           'ent', 'eront'
          )
      )


Créer des sessions
--------------------------

Le fichier définissant la session doit contenir une connaissance :code:`default`
donnant les paramètres de la session par défaut.
Cette connaissance définie aussi les attributs par défaut des questions.

.. code-block:: python

  Item('default',

     # Session attributes

     start_date = '2019-12-04', # Date of the opening of the session
     days = 120,                # Session duration counting holidays
     holidays = '',             # A day list as:  2019-12-21/16 2020-01-05
     weekend = '[5,6]',         # Not working saturday and sunday
     nr_days_to_answer = 3,     # Minimum value allowed is 1 day to answer
     time_scale = 1,            # Multiply all question time by this factor
     slot_x = 5,                # x*y give the number of questions
     slot_y = 3,                # to answer per day
     see_grades = False,        # See all students grades
     see_icons = False,         # See all students icons
     see_secrets = False,       # See all students secret keys
     see_activity = False,      # See working students now, minute, hour, day, week, month, more
     see_stats = False,         # See various statistics
     see_log = False,           # See the student log
     allow_exit = False,        # Terminate the server with '.../exit-safe' or '.../exit-unsafe'
     secret = '',               # To change the secret key
     fontFamily = 'sans',       # The page font, 'serif' may be a better choice
     # Replace string (JS regexp)
     # The replacements are done AFTER markdown formatting and HTML escaping
     replacements = r"[['\\bde +les\\b', 'des']]",
     # If a course has been canceled, asks the questions on the course latter.
     # All the first time question are asked is translated in time.
     # If there is one course per week, indicate 7
     nr_days_lag = 0,

     grade_max = 20,            # The maximum possible grade
     percent_bad_to_remove = 0, # If 10 then the 10% badest day grades are removed on grade export

     # Default question attributes

     first_day = 0,             # Ask the question the session's first day
     period = 1,                # Ask again the next day
     time = 10,                 # Ten seconds to answer
     if_bad = 2,                # Divide the period by 2 on bad answer
     # If the answer if good, there is the formula used to increase the period
     # period *= min( (1 + time / answer_time) * if_good, max_increase )
     if_good = 1,               # 0 to not increase the period
     max_increase = 4,          # Do not multiply too much
     collapse_spaces = 0,       # Multiples space collapses into one

     ########################################################################
     # DO NOT CHANGE ORDER
     ########################################################################
     competences = json.dumps((
        ('shell', 'Know how to use a shell.'),
        ('gcc', 'Know how to use a gcc compiler.'),
     ))
  )

Si l'identificateur de connaissance est un numéro d'étudiant au lieu de :code:`default`,
on peut lui mettre des paramètres spécifiques :

  * :code:`first_day` : la date de début de session ;
  * :code:`holidays` : les jours d'absences justifiées ;
  * :code:`time_scale` : mettre 1.33 pour les étudiants avec un tiers-temps
  * ou tout autre paramètre.

Les attributs :code:`see_grade`, :code:`see_secrets`, :code:`see_activity`, :code:`see_stats`
ajoutent des liens sur la page d'accueil du questionnaire.

Les notes sont recalculées à chaque lancement,
on peut donc les changer rétroactivement en changeant
le temps de réponse des questions.


Conseils
--------------------------

Pour les identificateurs :

  * Nommer les items en minuscule.
  * Les alternatives en majuscules.
  * Les questions avec une majuscule.
  * Garder les identificateurs court, car ils sont enregistrés
    dans l'historique de l'étudiant.
  * Ne donnez pas la réponse à la question dans l'identificateur.

Mettez les connaissances réutilisables dans un fichier ne contenant
pas de question.



Astuces
--------------------------

Si vous voulez que l'affichage de la connaissance :code:`{A}`
soit :code:`«A»` au lieu de :code:`A` il suffit de redéfinir l'attribut affiché :

.. code-block:: python

  Item("A", _key="«A»")


Sécurité
--------------------------

Il est impératif de cacher cette application derrière un serveur
web qui permettra l'accès via le protocol HTTPS.
En effet, les clefs d'API passent en clair dans l'URL.

S'il existe un utilisateur de la vie réelle nommé :code:`debug` pensez
à l'enlever du fichier par défaut de session car il a tous les droits.
Mais c'est lui qui est utilisé pour l'option :code:`--replace`
expliquée plus loin.



Modifier le questionnaire
--------------------------

Une fois le questionnaire modifié, il faut relancer le serveur
en attendant gentiment que les étudiants ne soient pas en train
de répondre à une question.

Pour cela, il suffit de lancer le serveur en ajoutant l'option
indiquant qu'il doit remplacer un serveur existant :
:code:`nohup ./server.py ...... --replace &`

Attention, l'URL doit être la même que pour l'ancien serveur
sinon le nouveau ne pourra arrêter l'ancien.




Les clefs d'API
--------------------------

Un utilisateur avec l'attribut :code:`see_secrets` à vrai utilise
un navigateur web pour aller sur l'URL : :code:`..../secrets/ACTION`
afin d'obtenir toutes les clefs d'API des utilisateurs pour
l'action indiquée.

Les clefs d'API sont utilisables seulement pour exécuter
l'action pour laquelle elles ont été créées,
elle ne peuvent pas faire autre chose.

Les clefs d'API peuvent être invalidées en changeant
l'attribut :code:`secret` de la session par défaut
ou de celle d'un utilisateur en particulier.
Elles sont aussi invalidées si l'inode du fichier de log change.


URL pour récupérer les notes des étudiants
..........................................

On génère les clefs API d'affichage des notes avec : :code:`..../secrets/grades`

::

  secret   secret/grades/3bef3c054adcd3a5dd73e356fa21ae07
  grades   grades/grades/962ac6777862152feadfedbd97ed0eb3
  icons    icons/grades/17da4e5c5ae42718ab3dddf34bd1173c
  ...

Les clefs d'API des utilisateurs qui n'ont pas le droit
de voir les notes ne seront pas utilisables.
L'utilisateur nommée :code:`grades` peut les voir,
on peut donc utiliser l'URL suivante sans authentification
pour récupérer les notes :

  :code:`..../_?ticket=grades/grades/962ac6777862152feadfedbd97ed0eb3`

URL pour récupérer les icones des étudiants
...........................................

Il faut pour cela récupérer la clef d'API permettant
d'obtenir toutes les clefs d'API des étudiants permettant
d'afficher l'icône de progression.

On utilise pour cela : :code:`..../secrets/secrets/icon.png`

::

  secret   secret/secrets/icon.png/7f0c6b8861eb07e5cb8b562c41d3da26
  grades   grades/secrets/icon.png/5c9b7c4fffc66fb3c1b43862d4d50b5b
  icons    icons/secrets/icon.png/0a61d0bb25c7d4ec3253517011cb9490
  ...

L'utilisateur :code:`secret` peut afficher les clefs secrètes.
C'est donc sa clef d'API que l'on pourra utiliser
dans TOMUSS par exemple :

:code:`..../_?ticket=secret/secrets/icon.png/7f0c6b8861eb07e5cb8b562c41d3da26`

::

  secret   secret/icon.png/7352ed2aa80c4988ea950c13dd5710f5
  grades   grades/icon.png/144ca2e806c12c984d7db1e824166478
  icons    icons/icon.png/cac8291e5a1534f7a58b669bfbf5eb86
  ...

Une fois cette liste de clefs d'API obtenue de manière automatique il suffit
d'ajouter :code:`..../_?ticket=` devant pour obtenir
les icônes de progression.


Sortie AMC (Auto Multiple Choice)
-----------------------------------

Il suffit de lancer :code:`./exam_mcq.py EXAM/*.py -- questions_to_not_display`

Si un entier est indiqué, c'est la graine de la série aléatoire.



TODO
--------------------------

Bug : Le rechargement perd le timer.

