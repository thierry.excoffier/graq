
LOGS=LOGS-DEMO

%.html:%.rst
	export LC_ALL=fr_FR.utf8 ; rst2html --title="$$(grep -A 1 ==== $*.rst | (read A ; read A ; echo "$$A"))" $*.rst $*.html

all: coverage mypy pylint

regtests:
	./regtests.py

regtests_server:
	./regtests_server.py

coverage:
	python3-coverage erase exam_mcq.py login.py server.py
	python3-coverage run regtests.py
	./regtests_server.py
	python3-coverage report exam_mcq.py login.py server.py
	python3-coverage html exam_mcq.py login.py server.py
	echo xdg-open "file:$$(pwd)/htmlcov/index.html"

mypy:
	mypy --ignore-missing-imports --python-version 3.6 exam_mcq.py login.py server.py

pylint:
	pylint exam_mcq.py login.py server.py regtests.py regtests_server.py

stats:
	@echo "$$(cat exam_mcq.py server.py login.py | wc -l) lines of Python"
	@echo "$$(cat mcq.js | wc -l) lines of JS"
	@echo "$$(cat mcq.html | wc -l) lines of HTML"
	@echo "$$(cat mcq.css | wc -l) lines of CSS"
	@echo "$$(cat regtests.py regtests_server.py TESTS*/* | wc -l) lines of Python for regtests"
	@echo "$$(cat documentation.rst | wc -l) lines of documentation"

clean:
	rm -rf xxx* htmlcov .mypy_cache __pycache__ LOGS-REGTESTS .coverage


documentation.html:documentation.rst

profile: # Profile the regression tests
	./regtests_server.py profile

start:
	mkdir $(LOGS) || true
	PORT=8080 ; \
	SERVER_LOG=log.$$(date '+%Y-%m-%d') ; \
	IP=$$(hostname -I | sed 's/ .*//') ; \
	CAS='' ; \
	if [ $$IP != '192.168.0.1' ] ; \
	then \
		IP="127.0.0.1" ; \
	fi ; \
	URL="http://$$IP:$$PORT" ; \
	./server.py \
		--port "$$PORT" \
		--ip "$$IP" \
		--url "$$URL" \
		--cas "$$CAS" \
		--session 'TESTS/session.py' \
		--logs '$(LOGS)' \
		--start_date "$$(date +%Y-%m-%d)" \
		--replace \
		--profile \
		START/start.py TESTS/animals.py TESTS/complex.py TESTS/mythical.py TESTS2/*.py  \
		>>$$SERVER_LOG 2>&1 </dev/null & \
	sleep 1 ; \
	echo "================== LOGS ======================" ; \
	tail -f $$SERVER_LOG
